﻿using Microsoft.Xna.Framework;

namespace ChrisXue.Pacman.Service
{
    public class Wall : StaticTile
    {
        public Wall(Level level, int x, int y) : base(level, x, y)
        {
            CanPassThrough = false;
        }
    }
}
