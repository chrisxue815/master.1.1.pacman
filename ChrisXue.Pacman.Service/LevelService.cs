﻿using System;
using System.ServiceModel;
using ChrisXue.Pacman.Contract;

namespace ChrisXue.Pacman.Service
{
    public partial class GameService : ILevelService
    {
        void ILevelService.Login(int id)
        {
            lock (Lobby.Players)
            {
                //TODO: check if id is still available
                Player = Lobby.Players[id];
                Player.LevelCallback = OperationContext.Current.GetCallbackChannel<ILevelServiceCallback>();
            }
            //OperationContext.Current.Channel.Closed += (sender, args) => LeaveRoom();
            OperationContext.Current.Channel.Faulted += (sender, args) => LeaveRoom();
        }

        public void Move(Point direction)
        {
            lock (Player.Level.PlayerControlled)
            {
                Player.Level.PlayerControlled[Player.Id].Move(direction);
            }
        }
    }
}
