﻿using System.Collections.Generic;

namespace ChrisXue.Pacman.Service
{
    public class Lobby
    {
        public Dictionary<int, Player> Players { get; set; }
        public int NextPlayerId { get; set; }
        public int MaxNumPlayers { get; set; }

        public Dictionary<int, Room> Rooms { get; set; }
        public int NextRoomId { get; set; }
        public int MaxNumRooms { get; set; }

        public static readonly Lobby Instance = new Lobby();

        private Lobby()
        {
            Players = new Dictionary<int, Player>();
            NextPlayerId = 0;
            MaxNumPlayers = 10;

            Rooms = new Dictionary<int, Room>();
            NextRoomId = 0;
            MaxNumRooms = 10;
        }

        public Player CreatePlayer(string name)
        {
            lock (this)
            {
                if (NextPlayerId > MaxNumPlayers) return null;

                var player = new Player(NextPlayerId, name);

                Players[NextPlayerId] = player;

                var i = NextPlayerId + 1;
                NextPlayerId = MaxNumPlayers;
                for (; i < MaxNumPlayers; i++)
                {
                    if (Players.ContainsKey(i)) continue;
                    NextPlayerId = i;
                    break;
                }

                return player;
            }
        }

        public void RemovePlayer(Player player)
        {
            player.LeaveRoom();
            Players[player.Id] = null;
            if (player.Id < NextPlayerId) NextPlayerId = player.Id;
        }

        public Room CreateRoom(Player host)
        {
            if (NextRoomId > MaxNumRooms) return null;

            var room = new Room(NextRoomId, host);

            Rooms[NextRoomId] = room;

            var i = NextRoomId + 1;
            NextRoomId = MaxNumRooms;
            for (; i < MaxNumRooms; i++)
            {
                if (Rooms.ContainsKey(i)) continue;
                NextRoomId = i;
                break;
            }

            return room;
        }

        public void RemoveRoom(Room room)
        {
            if (room.Id < NextRoomId) NextRoomId = room.Id;
            Rooms[room.Id] = null;
        }
    }
}
