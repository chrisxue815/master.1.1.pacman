﻿using System.ServiceModel;
using ChrisXue.Pacman.Contract;

namespace ChrisXue.Pacman.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Reentrant, UseSynchronizationContext = false)]
    public partial class GameService : ILobbyService
    {
        private Lobby Lobby { get; set; }
        private Player Player { get; set; }

        public GameService()
        {
            Lobby = Lobby.Instance;
        }

        public string ServerName()
        {
            Room room;
            if (!Lobby.Rooms.TryGetValue(0, out room))
            {
                return "";
            }

            lock (room)
            {
                return room.Host.Name;
            }
        }

        public int Signup(string playerName)
        {
            if (Player == null)
            {
                //TODO: return, check if exceeding max number of players
                Player = Lobby.CreatePlayer(playerName);
            }

            lock (Player)
            {
                //TODO: use wcf security instead of id to identify players
                return Player.Id;
            }
        }

        public LobbyInfo GetLobbyInfo()
        {
            var lobbyInfo = new LobbyInfo();
            foreach (var room in Lobby.Rooms)
            {
                lobbyInfo.Rooms[room.Key] = room.Value.Host.Name;
            }
            return lobbyInfo;
        }
    }
}
