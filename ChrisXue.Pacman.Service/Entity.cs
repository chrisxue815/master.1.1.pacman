﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ChrisXue.Pacman.Service
{
    public class Entity
    {
        public Level Game { get; set; }

        public Entity(Level game)
        {
            Game = game;
        }

        public virtual void Update()
        {
            
        }
    }
}
