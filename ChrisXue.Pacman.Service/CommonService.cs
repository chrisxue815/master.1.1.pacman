﻿using System.ServiceModel;
using ChrisXue.Pacman.Contract;

namespace ChrisXue.Pacman.Service
{
    public partial class GameService : ICommonService
    {
        public void LeaveRoom()
        {
            if (Player == null) return;
            Player.LeaveRoom();
        }

        public void LeaveLobby()
        {
            Player.LeaveLobby();
        }
    }
}
