﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ChrisXue.Pacman.Service
{
    public class StaticTile : Tile
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        public bool CanPassThrough { get; set; }

        public StaticTile(Level level, int x, int y) : base(level)
        {
            X = x;
            Y = y;
            CanPassThrough = true;
        }
    }
}
