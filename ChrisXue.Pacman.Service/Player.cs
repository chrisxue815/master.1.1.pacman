﻿using ChrisXue.Pacman.Contract;

namespace ChrisXue.Pacman.Service
{
    public class Player : PlayerInfo
    {
        public Room Room { get; set; }
        public Level Level { get { return Room.Level; } }
        public IRoomServiceCallback RoomCallback { get; set; }
        public ILevelServiceCallback LevelCallback { get; set; }

        public Player(int id, string name)
            : base(id, name)
        {
            Id = id;
            Name = name;
        }

        public void JoinRoom(Room room)
        {
            room.AddPlayer(this);
        }

        public void LeaveRoom()
        {
            if (Room == null) return;
            Room.RemovePlayer(this);
        }

        public void LeaveLobby()
        {
            Lobby.Instance.RemovePlayer(this);
        }
    }
}
