﻿using System.Collections.Generic;
using ChrisXue.Pacman.Contract;

namespace ChrisXue.Pacman.Service
{
    public class Room
    {
        public int Id { get; set; }
        public Player Host { get; set; }
        public Dictionary<int, Player> Players { get; set; }
        public List<int> NumPlayersInTeams { get; set; }
        public List<Dictionary<int, Player>> Teams { get; set; }
        public Level Level { get; set; }

        public Room(int id, Player host)
        {
            Id = id;
            Host = host;
            Players = new Dictionary<int, Player>();

            NumPlayersInTeams = new List<int> { 2, 4 };

            Teams = new List<Dictionary<int, Player>>();
            foreach (var numPlayersInTeam in NumPlayersInTeams)
            {
                Teams.Add(new Dictionary<int, Player>(numPlayersInTeam));
            }

            AddPlayer(host);
        }

        public void AddPlayer(Player player)
        {
            int teamId;
            int position;

            //TODO: return message
            if (!FindAvailablePosition(out teamId, out position)) return;

            lock (Teams)
            {
                Teams[teamId][position] = player;
            }

            if (player.Room != null)
            {
                player.Room.RemovePlayer(player);
            }

            RoomSlotInfo slotInfo;
            lock (player)
            {
                slotInfo = new RoomSlotInfo(player.Id, player.Name, teamId, position);
            }
            
            lock (Players)
            {
                foreach (var otherPlayer in Players.Values)
                {
                    lock (otherPlayer.RoomCallback)
                    {
                        otherPlayer.RoomCallback.PlayerJoinedRoom(slotInfo);
                    }
                }
                Players[player.Id] = player;
            }

            lock (player)
            {
                player.Room = this;
            }
        }

        private bool FindAvailablePosition(out int teamId, out int position)
        {
            lock (NumPlayersInTeams)
            {
                for (var i = 0; i < NumPlayersInTeams.Count; i++)
                {
                    for (var j = 0; j < NumPlayersInTeams[i]; j++)
                    {
                        lock (Teams)
                        {
                            if (Teams[i].ContainsKey(j)) continue;
                        }

                        teamId = i;
                        position = j;
                        return true;
                    }
                }
            }

            teamId = -1;
            position = -1;
            return false;
        }

        public void RemovePlayer(Player player)
        {
            lock (player.Room)
            {
                //TODO: change host
                //TODO: remove room after all players left
                if (player.Room != this) return;
            }

            lock (Players)
            {
                Players.Remove(player.Id);

                foreach (var otherPlayer in Players.Values)
                {
                    lock (otherPlayer.RoomCallback)
                    {
                        otherPlayer.RoomCallback.PlayerLeftRoom(player.Id);
                    }
                }
            }

            lock (player)
            {
                if (player.LevelCallback != null)
                {
                    lock (player.LevelCallback)
                    {
                        player.LevelCallback = null;
                    }
                }
                if (player.RoomCallback != null)
                {
                    lock (player.RoomCallback)
                    {
                        player.RoomCallback = null;
                    }
                }
                player.Room = null;
            }
        }

        public static implicit operator RoomInfo(Room room)
        {
            var roomInfo = new RoomInfo();
            var teams = room.Teams;

            lock (teams)
            {
                for (var teamId = 0; teamId < teams.Count; teamId++)
                {
                    var team = teams[teamId];
                    foreach (var slot in team)
                    {
                        var player = slot.Value;
                        var slotInfo = new RoomSlotInfo(player.Id, player.Name, teamId, slot.Key);
                        roomInfo.Slots.Add(slotInfo);
                    }
                }
            }

            return roomInfo;
        }
    }
}
