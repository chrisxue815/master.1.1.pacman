﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Point = ChrisXue.Pacman.Contract.Point;

namespace ChrisXue.Pacman.Service
{
    public class Ghost : DynamicTile
    {
        public Ghost(Level level, TileMap tileMap, int x, int y)
            : base(level, tileMap, x, y)
        {
        }

        public override void Move(Point newDirection)
        {
            if (newDirection.IsReverse(Direction))
            {
                PendingDirection = Point.Zero;
                return;
            }

            base.Move(newDirection);
        }
    }
}
