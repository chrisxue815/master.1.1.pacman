﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChrisXue.Pacman.Contract;

namespace ChrisXue.Pacman.Service
{
    public class DynamicTile : Tile
    {
        public const float Speed = 5f;

        public int X { get; set; }
        public int Y { get; set; }
        public Point Position
        {
            get { return new Point(X, Y); }
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }

        public int InitX { get; set; }
        public int InitY { get; set; }

        public Point InitPosition
        {
            get { return new Point(InitX, InitY); }
            set
            {
                InitX = value.X;
                InitY = value.Y;
            }
        }

        public TileMap TileMap { get; set; }
        public float Movement { get; set; }
        public Point Direction { get; set; }
        public Point PendingDirection { get; set; }

        public DynamicTile(Level level, TileMap tileMap, int x, int y)
            : base(level)
        {
            TileMap = tileMap;
            X = x;
            Y = y;
            InitX = x;
            InitY = y;
            tileMap.Add(this);
        }

        public override void Update()
        {
            var changingDirection =
                PendingDirection.NotZero() &&
                Movement <= 0 &&
                TileMap.CanPassThrough(Position, Position + PendingDirection);

            if (Direction.IsZero() && !changingDirection) return;

            Movement += Speed * Game.ElapsedSeconds;

            if (changingDirection && Movement >= 0)
            {
                Direction = PendingDirection;
                PendingDirection = Point.Zero;
            }
            else if (!TileMap.CanPassThrough(Position, Position + Direction) && Movement > 0)
            {
                Movement = 0;
                Direction = Point.Zero;
            }
            else if (Movement > 0.5f)
            {
                Movement -= 1;
                Position += Direction;
                Position = TileMap.Clamp(Position);
            }
        }

        public virtual void Move(Point newDirection)
        {
            if (newDirection == Direction)
            {
                PendingDirection = Point.Zero;
            }
            else if (newDirection.IsReverse(Direction))
            {
                Direction = newDirection;
                PendingDirection = Point.Zero;
                Movement = -Movement;
            }
            else
            {
                PendingDirection = newDirection;
            }
        }

        public void ResetPosition()
        {
            X = InitX;
            Y = InitY;
            Movement = 0;
            Direction = Point.Zero;
        }

        public bool Met(DynamicTile b)
        {
            if (!Position.IsFourConnected(b.Position)) return false;

            if (Math.Abs(Movement) > 0.25 || Math.Abs(b.Movement) > 0.25) return false;

            if (Movement > 0 == b.Movement > 0) return false;

            var toB = b.Position - Position;
            if (toB.IsPositive() == Movement > 0) return false;

            if (!Direction.IsReverse(toB) || b.Direction != toB) return false;

            return true;
        }
    }
}
