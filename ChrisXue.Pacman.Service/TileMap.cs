﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChrisXue.Pacman.Contract;

namespace ChrisXue.Pacman.Service
{
    public class TileMap : Entity, IEnumerable<Tile>
    {
        public Dictionary<int, Pacman> Pacmans { get; set; }
        public Dictionary<int, Ghost> Ghosts { get; set; }

        private int Width { get; set; }
        private int Height { get; set; }

        private StaticTile[,] StaticTiles { get; set; }
        private List<DynamicTile> DynamicTiles { get; set; }

        public TileMap(Level level, int width, int height)
            : base(level)
        {
            Width = width;
            Height = height;
            Pacmans = new Dictionary<int, Pacman>();
            Ghosts = new Dictionary<int, Ghost>();
            StaticTiles = new StaticTile[width, height];
            DynamicTiles = new List<DynamicTile>();
        }

        public override void Update()
        {
            foreach (var dynamicTile in DynamicTiles)
            {
                dynamicTile.Update();
            }

            foreach (var pacman in Pacmans.Values)
            {
                var pacmanPos = pacman.Position;
                var tile = this[pacmanPos];
                if (tile != null)
                {
                    if (tile is PowerPellet)
                    {
                        Game.EatenPowerPellet = true;
                        var now = Game.TotalSeconds;
                        var expiration = Game.PowerPelletExpiration;
                        var basicTime = now < expiration ? expiration : now;
                        Game.PowerPelletExpiration = basicTime + 10;
                    }
                    this[pacmanPos] = null;
                    Game.StateChanged.PelletsRemoved.Add(pacmanPos);
                }

                foreach (var ghost in Ghosts.Values)
                {
                    if (ghost.Position != pacmanPos && !ghost.Met(pacman)) continue;

                    if (Game.EatenPowerPellet)
                    {
                        // a pacman eating a ghost
                        ghost.ResetPosition();
                    }
                    else
                    {
                        // a ghost eating a pacman
                        Game.PacmanLives--;
                        if (Game.PacmanLives < 0)
                        {
                            
                        }
                        pacman.ResetPosition();
                        foreach (var ghost2 in Ghosts.Values)
                        {
                            ghost2.ResetPosition();
                        }
                        break;
                    }
                }
            }
        }

        public IEnumerator<Tile> GetEnumerator()
        {
            foreach (var staticTile in StaticTiles)
            {
                if (staticTile != null)
                {
                    yield return staticTile;
                }
            }

            foreach (var dynamicTile in DynamicTiles)
            {
                yield return dynamicTile;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public StaticTile this[int x, int y]
        {
            get { return StaticTiles[x, y]; }
            set { StaticTiles[x, y] = value; }
        }

        public StaticTile this[Point p]
        {
            get { return StaticTiles[p.X, p.Y]; }
            set { StaticTiles[p.X, p.Y] = value; }
        }

        public void Add(DynamicTile tile)
        {
            DynamicTiles.Add(tile);
        }

        public void Add(StaticTile tile)
        {
            StaticTiles[tile.X, tile.Y] = tile;
        }

        public void Add(Pellet pellet)
        {
            StaticTiles[pellet.X, pellet.Y] = pellet;
        }

        public bool CanPassThrough(Point walker, Point obstacle)
        {
            obstacle = Clamp(obstacle);
            var tile = this[obstacle];

            if (tile is Gate)
            {
                return walker.IsBelow(obstacle) || walker.IsHorizontal(obstacle);
            }
            else
            {
                return tile == null || tile.CanPassThrough;
            }
        }

        public int ClampX(int x)
        {
            if (x < 0) x += Width;
            else if (x >= Width) x -= Width;
            return x;
        }

        public int ClampY(int y)
        {
            if (y < 0) y += Height;
            else if (y >= Height) y -= Height;
            return y;
        }

        public Point Clamp(Point p)
        {
            return new Point(ClampX(p.X), ClampY(p.Y));
        }
    }
}
