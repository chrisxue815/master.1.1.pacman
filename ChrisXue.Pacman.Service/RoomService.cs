﻿using System;
using System.Linq;
using System.ServiceModel;
using ChrisXue.Pacman.Contract;

namespace ChrisXue.Pacman.Service
{
    public partial class GameService : IRoomService
    {
        void IRoomService.Login(int id)
        {
            lock (Lobby.Players)
            {
                //TODO: check if id is still available
                Player = Lobby.Players[id];
                Player.RoomCallback = OperationContext.Current.GetCallbackChannel<IRoomServiceCallback>();
            }
            //OperationContext.Current.Channel.Closed += (sender, args) => LeaveRoom();
            OperationContext.Current.Channel.Faulted += (sender, args) => LeaveRoom();
        }

        public RoomInfo CreateRoom()
        {
            //TODO: return, check if exceeding max number of rooms
            //TODO: set max num rooms = 1 if it's a lan game
            return Lobby.CreateRoom(Player);
        }

        public RoomInfo JoinFirstRoom()
        {
            return JoinRoom(0);
        }

        public RoomInfo JoinRoom(int id)
        {
            Room room;
            lock (Lobby.Rooms)
            {
                //TODO: return, check id, check available
                room = Lobby.Rooms[id];
            }

            Player.JoinRoom(room);
            return room;
        }

        public void ChangePosition()
        {
            throw new NotImplementedException();
        }

        public void SetMap(string mapName)
        {
            throw new NotImplementedException();
        }

        public RoomInfo GetRoomInfo()
        {
            lock (Player.Room)
            {
                return Player.Room;
            }
        }

        public void StartGame(int mapId)
        {
            var mapPath = new string[3];
            mapPath[0] = "Content/maps/1980.tmx";
            mapPath[1] = "Content/maps/ms-pacman.tmx";
            mapPath[2] = "Content/maps/google.tmx";
            if (mapId < 0 || mapId > 2) mapId = 2;

            lock (Player.Room)
            {
                var startingInfo = new GameStartingInfo
                {
                    MapPath = mapPath[mapId],
                    NumPacman = 2,
                    NumGhosts = 4,
                };
                startingInfo.PacmanMaxLives = 2 * startingInfo.NumPacman;
                Player.Room.Level = new Level(Player.Room, startingInfo);
                Player.Room.Level.Start();
                foreach (var player in Player.Room.Players.Values)
                {
                    player.RoomCallback.GameStarted(startingInfo);
                }
            }
        }
    }
}
