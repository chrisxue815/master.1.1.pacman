﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChrisXue.Pacman.Contract;

namespace ChrisXue.Pacman.Service
{
    public class GhostAgent : Entity
    {
        private Ghost Ghost { get; set; }
        private float next;
        private static Random Random { get; set; }

        public GhostAgent(Level level, Ghost ghost) : base(level)
        {
            Ghost = ghost;
            next = Game.TotalSeconds + 1;
            Random = new Random();
        }

        public override void Update()
        {
            if (Game.TotalSeconds > next)
            {
                next = Game.TotalSeconds + 1;
                Ghost.Move(Point.RandomDirection());
            }
        }
    }
}
