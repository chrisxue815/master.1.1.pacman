﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using ChrisXue.Pacman.Contract;
using TiledSharp;

namespace ChrisXue.Pacman.Service
{
    public class Level
    {
        public Room Room { get; set; }
        public GameStartingInfo StartingInfo { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }

        private TileMap TileMap { get; set; }
        private List<Entity> Entities { get; set; }
        public bool EatenPowerPellet { get; set; }
        public float PowerPelletExpiration { get; set; }

        public Dictionary<int, DynamicTile> PlayerControlled { get; set; }

        public float ElapsedSeconds { get; set; }
        public float TotalSeconds { get; set; }
        public int PacmanMaxLives { get; set; }
        public int PacmanLives { get; set; }

        public Thread Thread { get; set; }
        public bool Running { get; set; }

        public LevelInfo StateChanged { get; set; }

        public enum TileSetGid
        { Empty, Pacman, Blinky, Inky, Pinky, Clyde, Wall, Gate, Pellet = 11, PowerPellet }

        public Level(Room room, GameStartingInfo startingInfo)
        {
            Room = room;
            StartingInfo = startingInfo;

            var map = new TmxMap(StartingInfo.MapPath);

            var tiles = map.Layers[0].Tiles;

            Width = map.Width;
            Height = map.Height;
            TileMap = new TileMap(this, Width, Height);
            Entities = new List<Entity> { TileMap };
            EatenPowerPellet = false;
            PacmanMaxLives = startingInfo.PacmanMaxLives;
            PacmanLives = PacmanMaxLives;

            var pacmandId = 0;
            foreach (var tile in tiles)
            {
                var gid = (TileSetGid)tile.Gid;
                var x = tile.X;
                var y = tile.Y;
                var ghostId = -1;

                switch (gid)
                {
                    case TileSetGid.Wall:
                        TileMap.Add(new Wall(this, x, y));
                        break;
                    case TileSetGid.Pellet:
                        TileMap.Add(new Pellet(this, x, y));
                        break;
                    case TileSetGid.PowerPellet:
                        TileMap.Add(new PowerPellet(this, x, y));
                        break;
                    case TileSetGid.Pacman:
                        TileMap.Pacmans[pacmandId] = new Pacman(this, TileMap, x, y);
                        pacmandId++;
                        break;
                    case TileSetGid.Blinky:
                        ghostId = 0;
                        break;
                    case TileSetGid.Inky:
                        ghostId = 1;
                        break;
                    case TileSetGid.Pinky:
                        ghostId = 2;
                        break;
                    case TileSetGid.Clyde:
                        ghostId = 3;
                        break;
                    case TileSetGid.Gate:
                        TileMap.Add(new Gate(this, x, y));
                        break;
                    default:
                        break;
                }

                if (ghostId == -1) continue;

                var ghost = new Ghost(this, TileMap, x, y);
                TileMap.Ghosts[ghostId] = ghost;

                if (room.Teams[1].ContainsKey(ghostId)) continue;

                var ghostAgent = new GhostAgent(this, ghost);
                Entities.Add(ghostAgent);
            }

            PlayerControlled = new Dictionary<int, DynamicTile>();
            var teams = room.Teams;

            for (var teamId = 0; teamId < teams.Count; teamId++)
            {
                var team = teams[teamId];
                foreach (var slot in team)
                {
                    var position = slot.Key;
                    var player = slot.Value;
                    if (teamId == 0)
                    {
                        PlayerControlled[player.Id] = TileMap.Pacmans[position];
                    }
                    else
                    {
                        PlayerControlled[player.Id] = TileMap.Ghosts[position];
                    }
                }
            }
        }

        public void Start()
        {
            Thread = new Thread(() =>
            {
                const float interval = 1 / 60f;
                var startTime = DateTime.Now;
                var nextFrame = startTime.AddSeconds(interval);
                for (; ; )
                {
                    if (Room.Players.Count == 0) break;
                    var now = DateTime.Now;
                    if (now < nextFrame) continue;

                    ElapsedSeconds = (float)(now - nextFrame).TotalSeconds + interval;
                    TotalSeconds = (float)(now - startTime).TotalSeconds;
                    nextFrame = now.AddSeconds(interval);

                    Update();
                }
            });

            Thread.Start();
        }

        public void Update()
        {
            StateChanged = new LevelInfo();

            foreach (var entity in Entities)
            {
                entity.Update();
            }

            StateChanged.PacmanLives = PacmanLives;

            if (EatenPowerPellet && PowerPelletExpiration <= TotalSeconds)
            {
                EatenPowerPellet = false;
            }
            StateChanged.EatenPowerPellet = EatenPowerPellet;

            foreach (var pacman in TileMap.Pacmans.Values)
            {
                var info = new DynamicTileInfo
                {
                    Position = pacman.Position,
                    Direction = pacman.Direction,
                    Movement = pacman.Movement
                };
                StateChanged.Pacmans.Add(info);
            }

            foreach (var ghost in TileMap.Ghosts.Values)
            {
                var info = new DynamicTileInfo
                {
                    Position = ghost.Position,
                    Direction = ghost.Direction,
                    Movement = ghost.Movement
                };
                StateChanged.Ghosts.Add(info);
            }

            foreach (var tile in PlayerControlled)
            {
                Player player;
                if (!Room.Players.TryGetValue(tile.Key, out player)) continue;
                if (player.LevelCallback == null) continue;

                try
                {
                    lock (player.LevelCallback)
                    {
                        player.LevelCallback.StateChanged(StateChanged);
                    }
                }
                catch (CommunicationObjectAbortedException ex)
                {
                    player.LeaveLobby();
                }
                catch (ObjectDisposedException ex)
                {
                }
            }
        }
    }
}
