﻿using System;
using System.ServiceModel;
using ChrisXue.Pacman.Service;

namespace ChrisXue.Pacman.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var gameServer = new GameServer())
            {
                gameServer.Open();

                Console.Read();
            }
        }
    }
}
