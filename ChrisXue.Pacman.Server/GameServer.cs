﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Discovery;
using ChrisXue.Pacman.Contract;
using ChrisXue.Pacman.Service;

namespace ChrisXue.Pacman.Server
{
    public class GameServer : IDisposable
    {
        private ServiceHost ServiceHost { get; set; }

        public static Dictionary<Type, string> Pathes { get; set; }
        static GameServer()
        {
            Pathes = new Dictionary<Type, string>();
            Pathes[typeof(ILobbyService)] = "lobby";
            Pathes[typeof(IRoomService)] = "room";
            Pathes[typeof(ILevelService)] = "level";
        }

        public Dictionary<Type, EndpointAddress> Addresses { get; set; }

        public GameServer()
        {
            Addresses = new Dictionary<Type, EndpointAddress>();
            var address = string.Format("net.tcp://{0}:51725", Environment.MachineName);

            ServiceHost = new ServiceHost(typeof(GameService), new Uri(address));

            var binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.None;

            // add lobby service
            AddServiceEndoint(typeof(ILobbyService), binding);

            // add room service
            AddServiceEndoint(typeof(IRoomService), binding);

            // add level service
            AddServiceEndoint(typeof(ILevelService), binding);

            // add metadata exchange
            var mexTcpBinding = MetadataExchangeBindings.CreateMexTcpBinding();
            ServiceHost.Description.Behaviors.Add(new ServiceMetadataBehavior());
            ServiceHost.AddServiceEndpoint(typeof(IMetadataExchange), mexTcpBinding, "mex");

            // add discovery
            ServiceHost.Description.Behaviors.Add(new ServiceDiscoveryBehavior());
            ServiceHost.AddServiceEndpoint(new UdpDiscoveryEndpoint());

            // debug behavior
            //TODO: improve this, add server-side exception logging and return less exception info to clients
            var debugBehavior = ServiceHost.Description.Behaviors.Find<ServiceDebugBehavior>();
            if (debugBehavior == null)
            {
                debugBehavior = new ServiceDebugBehavior { IncludeExceptionDetailInFaults = true };
                ServiceHost.Description.Behaviors.Add(debugBehavior);
            }
            else
            {
                debugBehavior.IncludeExceptionDetailInFaults = true;
            }
        }

        private void AddServiceEndoint(Type type, Binding binding)
        {
            var serviceEndpoint = ServiceHost.AddServiceEndpoint(type, binding, Pathes[type]);
            Addresses[type] = serviceEndpoint.Address;
        }

        public void Open()
        {
            try
            {
                //TODO: catch port already being used exception
                ServiceHost.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }

        }

        public event EventHandler Opened
        {
            add { ServiceHost.Opened += value; }
            remove { ServiceHost.Opened -= value; }
        }

        public void Close()
        {
            try
            {
                //TODO: check why servicehost.close is slow
                ServiceHost.Abort();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public event EventHandler Closed
        {
            add { ServiceHost.Closed += value; }
            remove { ServiceHost.Closed -= value; }
        }

        void IDisposable.Dispose()
        {
            ServiceHost.Close();
        }
    }
}
