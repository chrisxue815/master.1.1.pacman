﻿using System.ServiceModel;

namespace ChrisXue.Pacman.Contract
{
    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(IRoomServiceCallback))]
    public interface IRoomService : ICommonService
    {
        [OperationContract]
        void Login(int id);

        [OperationContract]
        RoomInfo CreateRoom();

        [OperationContract]
        RoomInfo JoinFirstRoom();

        [OperationContract]
        RoomInfo JoinRoom(int id);

        [OperationContract]
        void ChangePosition();

        [OperationContract]
        void SetMap(string mapName);

        [OperationContract]
        RoomInfo GetRoomInfo();

        [OperationContract(IsOneWay = true)]
        void StartGame(int mapId);
    }
}
