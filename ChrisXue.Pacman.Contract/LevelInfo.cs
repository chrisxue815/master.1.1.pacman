﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ChrisXue.Pacman.Contract
{
    // level update info
    [DataContract]
    public class LevelInfo
    {
        [DataMember]
        public List<Point> PelletsRemoved { get; set; }

        [DataMember]
        public List<DynamicTileInfo> Ghosts { get; set; }

        [DataMember]
        public List<DynamicTileInfo> Pacmans { get; set; }

        [DataMember]
        public bool EatenPowerPellet { get; set; }

        [DataMember]
        public int PacmanLives { get; set; }

        public LevelInfo()
        {
            PelletsRemoved = new List<Point>();
            Ghosts = new List<DynamicTileInfo>();
            Pacmans = new List<DynamicTileInfo>();
            EatenPowerPellet = false;
        }
    }
}
