﻿using System.Runtime.Serialization;

namespace ChrisXue.Pacman.Contract
{
    [DataContract]
    public class DynamicTileInfo
    {
        [DataMember]
        public Point Position { get; set; }

        [DataMember]
        public float Movement { get; set; }

        [DataMember]
        public Point Direction { get; set; }
    }
}
