﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ChrisXue.Pacman.Contract
{
    [DataContract]
    public class RoomInfo
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public List<RoomSlotInfo> Slots { get; set; }

        public RoomInfo()
        {
            Slots = new List<RoomSlotInfo>();
        }
    }
}
