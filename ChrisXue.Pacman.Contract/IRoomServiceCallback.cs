﻿using System.ServiceModel;

namespace ChrisXue.Pacman.Contract
{
    public interface IRoomServiceCallback
    {
        [OperationContract(IsOneWay = true)]
        void PlayerJoinedRoom(RoomSlotInfo roomSlotInfo);

        [OperationContract(IsOneWay = true)]
        void PlayerLeftRoom(int id);

        [OperationContract(IsOneWay = true)]
        void PlayerChangedPosition(RoomSlotInfo roomSlotInfo);

        [OperationContract(IsOneWay = true)]
        void GameStarted(GameStartingInfo startingInfo);
    }
}
