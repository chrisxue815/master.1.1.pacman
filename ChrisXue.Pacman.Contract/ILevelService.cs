﻿using System.ServiceModel;

namespace ChrisXue.Pacman.Contract
{
    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(ILevelServiceCallback))]
    public interface ILevelService : ICommonService
    {
        [OperationContract]
        void Login(int id);

        [OperationContract(IsOneWay = true)]
        void Move(Point direction);
    }
}
