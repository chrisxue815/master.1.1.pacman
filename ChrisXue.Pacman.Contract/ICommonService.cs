﻿using System.ServiceModel;

namespace ChrisXue.Pacman.Contract
{
    [ServiceContract]
    public interface ICommonService
    {
        [OperationContract]
        void LeaveRoom();

        [OperationContract]
        void LeaveLobby();
    }
}
