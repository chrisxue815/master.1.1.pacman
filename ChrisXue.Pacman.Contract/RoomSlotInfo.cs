﻿using System.Runtime.Serialization;

namespace ChrisXue.Pacman.Contract
{
    [DataContract]
    public class RoomSlotInfo
    {
        [DataMember]
        public PlayerInfo Player { get; set; }

        [DataMember]
        public int TeamId { get; set; }

        [DataMember]
        public int Position { get; set; }

        public RoomSlotInfo(int playerId, string playerName, int teamId, int position)
        {
            Player = new PlayerInfo(playerId, playerName);
            TeamId = teamId;
            Position = position;
        }

        public override string ToString()
        {
            return Player.Name;
        }
    }
}
