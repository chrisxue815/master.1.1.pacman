﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace ChrisXue.Pacman.Contract
{
    public interface ILevelServiceCallback
    {
        [OperationContract(IsOneWay = true)]
        void StateChanged(LevelInfo levelInfo);
    }
}
