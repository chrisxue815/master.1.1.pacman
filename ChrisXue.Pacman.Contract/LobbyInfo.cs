﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChrisXue.Pacman.Contract
{
    [DataContract]
    public class LobbyInfo
    {
        [DataMember]
        public Dictionary<int, string> Rooms { get; set; }
    }
}
