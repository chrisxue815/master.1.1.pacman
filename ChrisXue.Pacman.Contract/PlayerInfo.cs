﻿using System.Runtime.Serialization;

namespace ChrisXue.Pacman.Contract
{
    [DataContract]
    public class PlayerInfo
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        public PlayerInfo(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
