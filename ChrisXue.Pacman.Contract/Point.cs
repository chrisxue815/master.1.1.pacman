﻿using System;
using Microsoft.Xna.Framework;

namespace ChrisXue.Pacman.Contract
{
    public struct Point
    {
        public int X;
        public int Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Point Zero
        {
            get { return new Point(0, 0); }
        }

        public static Point Left
        {
            get { return new Point(-1, 0); }
        }

        public static Point Right
        {
            get { return new Point(1, 0); }
        }

        public static Point Up
        {
            get { return new Point(0, -1); }
        }

        public static Point Down
        {
            get { return new Point(0, 1); }
        }

        private static readonly Random Random = new Random();
        public static Point RandomDirection()
        {
            switch (Random.Next(4))
            {
                case 0:
                    return Up;
                case 1:
                    return Down;
                case 2:
                    return Left;
                case 3:
                    return Right;
                default:
                    return Zero;
            }
        }

        public Point Reverse
        {
            get { return new Point(-X, -Y); }
        }

        public bool IsAbove(Point p)
        {
            return Y < p.Y;
        }

        public bool IsBelow(Point p)
        {
            return Y > p.Y;
        }

        public bool IsHorizontal(Point p)
        {
            return Y == p.Y;
        }

        public bool IsLeft(Point p)
        {
            return X < p.X;
        }

        public bool IsRight(Point p)
        {
            return X > p.X;
        }

        public bool IsVertical(Point p)
        {
            return X == p.X;
        }

        public bool IsFourConnected(Point p)
        {
            return X == p.X && Math.Abs(Y - p.Y) == 1
                   || Y == p.Y && Math.Abs(X - p.X) == 1;
        }

        public bool IsPositive()
        {
            return X >= 0 && Y >= 0;
        }

        public bool IsNegative()
        {
            return X <= 0 && Y <= 0;
        }

        public static implicit operator Vector2(Point p)
        {
            return new Vector2(p.X, p.Y);
        }

        public static bool operator ==(Point p1, Point p2)
        {
            return p1.X == p2.X && p1.Y == p2.Y;
        }

        public static bool operator !=(Point p1, Point p2)
        {
            return !(p1 == p2);
        }

        public static Point operator +(Point p1, Point p2)
        {
            return new Point(p1.X + p2.X, p1.Y + p2.Y);
        }

        public static Point operator -(Point p1, Point p2)
        {
            return new Point(p1.X - p2.X, p1.Y - p2.Y);
        }

        public static Point operator -(Point p)
        {
            return new Point(-p.X, -p.Y);
        }

        public static Point operator *(Point p, int scale)
        {
            return new Point(p.X * scale, p.Y * scale);
        }

        public static Point operator *(int scale, Point p)
        {
            return new Point(p.X * scale, p.Y * scale);
        }

        public static Vector2 operator *(Point p, float scale)
        {
            return new Vector2(p.X * scale, p.Y * scale);
        }

        public static Vector2 operator *(float scale, Point p)
        {
            return new Vector2(p.X * scale, p.Y * scale);
        }

        public static Vector2 operator *(Point p1, Point p2)
        {
            return new Vector2(p1.X * p2.X, p1.Y * p2.Y);
        }

        public static int Dot(Point p1, Point p2)
        {
            return p1.X * p2.X + p1.Y * p2.Y;
        }

        public static Point operator /(Point p, int scale)
        {
            return new Point(p.X / scale, p.Y / scale);
        }

        public static Vector2 operator /(Point p, float scale)
        {
            return new Vector2(p.X / scale, p.Y / scale);
        }

        public float Length()
        {
            return (float)Math.Sqrt(X * X + Y * Y);
        }

        public int LengthSquared()
        {
            return X * X + Y * Y;
        }

        public static float Distance(Point p1, Point p2)
        {
            return (p1 - p2).Length();
        }

        public bool IsZero()
        {
            return X == 0 && Y == 0;
        }

        public bool NotZero()
        {
            return X != 0 || Y != 0;
        }

        public bool IsReverse(Point p)
        {
            return X == -p.X && Y == -p.Y;
        }

        public bool IsPerpendicular(Point p)
        {
            return Dot(this, p) == 0;
        }

        public bool IsNonZeroPerpendicular(Point p)
        {
            return X != 0 && Y != 0 && p.X != 0 && p.Y != 0 && Dot(this, p) == 0;
        }

        public bool Equals(Point other)
        {
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Point && Equals((Point)obj);
        }

        public override int GetHashCode()
        {
            return X.GetHashCode() + Y.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{{X:{0} Y:{1}}}", X, Y);
        }
    }
}
