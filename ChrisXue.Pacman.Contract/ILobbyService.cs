﻿using System.Collections.Generic;
using System.ServiceModel;

namespace ChrisXue.Pacman.Contract
{
    [ServiceContract]
    public interface ILobbyService : ICommonService
    {
        [OperationContract]
        string ServerName();

        [OperationContract]
        int Signup(string playerName);

        [OperationContract]
        LobbyInfo GetLobbyInfo();
    }
}
