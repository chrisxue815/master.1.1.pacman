﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChrisXue.Pacman.Contract
{
    [DataContract]
    public class GameStartingInfo
    {
        [DataMember]
        public string MapPath { get; set; }

        [DataMember]
        public int NumPacman { get; set; }

        [DataMember]
        public int NumGhosts { get; set; }

        [DataMember]
        public int PacmanMaxLives { get; set; }
    }
}
