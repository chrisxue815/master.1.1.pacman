﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ChrisXue.Pacman.Client
{
    public class Gate : StaticTile
    {
        public Gate(int x, int y) : base(x, y)
        {
            DiffuseColor = Color.Gray.ToVector3();
        }
    }
}
