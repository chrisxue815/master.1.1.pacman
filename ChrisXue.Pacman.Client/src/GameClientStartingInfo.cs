﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChrisXue.Pacman.Contract;

namespace ChrisXue.Pacman.Client
{
    public class GameClientStartingInfo : GameStartingInfo
    {
        public EndpointAddress RoomAddress { get; set; }
        public EndpointAddress LevelAddress { get; set; }
        public int Team { get; set; }
        public int Position { get; set; }
        public int PlayerId { get; set; }
    }
}
