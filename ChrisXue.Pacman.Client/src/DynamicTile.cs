﻿using Microsoft.Xna.Framework;
using Point = ChrisXue.Pacman.Contract.Point;

namespace ChrisXue.Pacman.Client
{
    public class DynamicTile : Tile
    {
        public const float Speed = 5f;

        public int X { get; set; }
        public int Y { get; set; }
        public Point Position
        {
            get { return new Point(X, Y); }
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }

        public int InitX { get; set; }
        public int InitY { get; set; }

        public Point InitPosition
        {
            get { return new Point(InitX, InitY); }
            set
            {
                InitX = value.X;
                InitY = value.Y;
            }
        }

        public TileMap TileMap { get; set; }
        public float Movement { get; set; }
        public Point Direction { get; set; }

        public DynamicTile(TileMap tileMap, int x, int y)
        {
            TileMap = tileMap;
            X = x;
            Y = y;
            InitX = x;
            InitY = y;
            tileMap.Add(this);
            SetWorldPosition();
        }

        public override void Update()
        {
            Movement += Game.ElapsedSeconds * Speed;
        }

        public virtual void Move(Point newDirection)
        {
            Game.Level.Service.Move(newDirection);
        }

        public void ResetPosition()
        {
            X = InitX;
            Y = InitY;
            Movement = 0;
            Direction = Point.Zero;
            SetWorldPosition();
        }

        public void SetWorldPosition()
        {
            WorldPosition = new Vector3(Width * X + Movement * Direction.X, 0, Width * Y + Movement * Direction.Y);
        }
    }
}
