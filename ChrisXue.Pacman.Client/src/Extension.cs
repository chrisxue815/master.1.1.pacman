﻿using System;
using Microsoft.Xna.Framework;
using Point = ChrisXue.Pacman.Contract.Point;

namespace ChrisXue.Pacman.Client
{
    public static class Extensions
    {
        public static readonly Vector2 Left = new Vector2(-1, 0);
        public static readonly Vector2 Right = new Vector2(1, 0);
        public static readonly Vector2 Up = new Vector2(0, -1);
        public static readonly Vector2 Down = new Vector2(0, 1);

        public static Point ToPoint(this Vector2 vec)
        {
            return new Point((int) Math.Round(vec.X), (int) Math.Round(vec.Y));
        }
    }
}
