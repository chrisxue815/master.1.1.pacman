using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Windows.Forms;
using ChrisXue.Pacman.Client.UI;
using ChrisXue.Pacman.Contract;
using ChrisXue.Pacman.Server;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Keys = Microsoft.Xna.Framework.Input.Keys;

namespace ChrisXue.Pacman.Client
{
    public class Game1 : BaseGame
    {
        public static Game1 Instance;

        public Camera Camera { get; set; }
        public Level Level { get; set; }
        public int Width { get { return GraphicsDevice.Viewport.Width; } }
        public int Height { get { return GraphicsDevice.Viewport.Height; } }

        public GameClientStartingInfo StartingInfo { get; set; }

        private List<Entity> Entities { get; set; }
        private Form1 Form { get; set; }
        private bool Running { get; set; }

        public Game1(Form1 form)
        {
            Instance = this;
            Form = form;

            var xnaWindow = Control.FromHandle(Window.Handle);
            xnaWindow.GotFocus += (sender, args) =>
            {
                var xnaForm = sender as Form;
                if (xnaForm != null) xnaForm.Visible = false;
                form.TopMost = false;
            };

            Graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width,
                PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height,
                PreferMultiSampling = true,
                SynchronizeWithVerticalRetrace = true
            };

            Graphics.PreparingDeviceSettings += (sender, e) =>
            {
                e.GraphicsDeviceInformation.PresentationParameters.DeviceWindowHandle = Form.GamePanel.Handle;
            };

            Graphics.ApplyChanges();

            Running = false;
        }

        public void Initialize(GameClientStartingInfo startingInfo)
        {
            StartingInfo = startingInfo;
            startingInfo.LevelAddress = GetLevelAddress(startingInfo.RoomAddress);

            Entities.Clear();

            Level = new Level();
            Entities.Add(Level);

            Camera = new Camera();
            Entities.Add(Camera);

            Entities.Add(new Monitor());

            LoadContent();

            Running = true;
        }

        private static EndpointAddress GetLevelAddress(EndpointAddress roomAddress)
        {
            var levelPath = GameServer.Pathes[typeof(ILevelService)];
            var uriBuilder = new UriBuilder(roomAddress.Uri) { Path = levelPath };
            var uri = uriBuilder.Uri;
            var levelAddress = new EndpointAddress(uri);
            return levelAddress;
        }

        protected override void Initialize()
        {
            Entities = new List<Entity>();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            foreach (var entity in Entities)
            {
                entity.LoadContent();
            }

            base.LoadContent();
        }

        protected override void UnloadContent()
        {
            foreach (var entity in Entities)
            {
                entity.UnloadContent();
            }

            base.UnloadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            if (!Running) return;

            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                Running = false;
                UnloadContent();
                Form.ShowMainMenu();
            }

            foreach (var entity in Entities)
            {
                entity.Update();
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            if (!Running) return;

            GraphicsDevice.Clear(Color.Black);

            GraphicsDevice.DepthStencilState = DepthStencilState.Default;

            foreach (var entity in Entities)
            {
                entity.Draw();
            }

            base.Draw(gameTime);
        }

        public void Quit()
        {
            UnloadContent();
            Exit();
        }
    }
}
