﻿using System.Linq;
using ChrisXue.Pacman.Contract;
using Microsoft.Xna.Framework.Input;

namespace ChrisXue.Pacman.Client
{
    public class Player : Entity
    {
        private DynamicTile Controlled { get; set; }
        private Keys[] PreviousKeys { get; set; }

        public Player(DynamicTile controlled)
        {
            Controlled = controlled;
            PreviousKeys = new Keys[0];
        }

        public override void Update()
        {
            var keys = Keyboard.GetState().GetPressedKeys();

            foreach (var key in keys.Where(key => !PreviousKeys.Contains(key)))
            {
                switch (key)
                {
                    case Keys.Left:
                        Controlled.Move(Point.Left);
                        break;
                    case Keys.Right:
                        Controlled.Move(Point.Right);
                        break;
                    case Keys.Up:
                        Controlled.Move(Point.Up);
                        break;
                    case Keys.Down:
                        Controlled.Move(Point.Down);
                        break;
                }
            }

            PreviousKeys = keys;
        }
    }
}
