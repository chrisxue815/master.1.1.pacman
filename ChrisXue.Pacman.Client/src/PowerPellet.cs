﻿using Microsoft.Xna.Framework;

namespace ChrisXue.Pacman.Client
{
    public class PowerPellet : StaticTile
    {
        public PowerPellet(int x, int y) : base(x, y)
        {
            ModelName = "models/sphere";
            LocalTransform = Matrix.CreateScale(0.5f);
            DiffuseColor = new Color(255, 184, 151).ToVector3();
        }
    }
}
