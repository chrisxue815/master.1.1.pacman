﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ChrisXue.Pacman.Client
{
    public class Entity
    {
        protected Game1 Game { get; set; }
        public Vector3 WorldPosition { get; set; }
        protected Model Model { get; set; }
        protected string ModelName { get; set; }
        protected Matrix LocalTransform { get; set; }
        public Vector3 DiffuseColor { get; set; }

        public Entity()
        {
            Game = Game1.Instance;
            LocalTransform = Matrix.Identity;
        }

        public virtual void LoadContent()
        {
            if (ModelName == null) return;
            Model = Game.Content.Load<Model>(ModelName);
        }

        public virtual void UnloadContent()
        {
        }

        public virtual void Update()
        {
        }

        public virtual void Draw()
        {
            if (Model == null) return;

            foreach (var mesh in Model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.DiffuseColor = DiffuseColor;
                    effect.World = LocalTransform * Matrix.CreateTranslation(WorldPosition);
                    effect.View = Game.Camera.View;
                    effect.Projection = Game.Camera.Projection;
                }

                mesh.Draw();
            }
        }
    }
}
