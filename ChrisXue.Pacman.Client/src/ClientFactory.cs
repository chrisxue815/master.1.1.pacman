﻿using System.ServiceModel;
using ChrisXue.Pacman.Contract;

namespace ChrisXue.Pacman.Client
{
    public static class ClientFactory
    {
        public static ILobbyService CreateLobbyClient(EndpointAddress lobbyAddress)
        {
            var binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.None;

            var lobbyService = ChannelFactory<ILobbyService>.CreateChannel(binding, lobbyAddress);

            return lobbyService;
        }

        public static IRoomService CreateRoomClient(EndpointAddress roomAddress, IRoomServiceCallback callback)
        {
            var instanceContext = new InstanceContext(callback);
            var binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.None;

            var roomService = DuplexChannelFactory<IRoomService>.CreateChannel(instanceContext, binding, roomAddress);

            return roomService;
        }

        public static ILevelService CreateLevelClient(EndpointAddress levelAddress, ILevelServiceCallback callback)
        {
            var instanceContext = new InstanceContext(callback);
            var binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.None;

            var levelService = DuplexChannelFactory<ILevelService>.CreateChannel(instanceContext, binding, levelAddress);

            return levelService;
        }
    }
}
