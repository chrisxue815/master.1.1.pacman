﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChrisXue.Pacman.Client
{
    public class Tile : Entity
    {
        public const float Width = 1;
        public const float HalfWidth = Width / 2;

        public Tile()
        {
            ModelName = "models/cube";
        }

        public virtual void Collided(Tile b)
        {
        }
    }
}
