﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ChrisXue.Pacman.Client
{
    public class Pellet : StaticTile
    {
        public Pellet(int x, int y) : base(x, y)
        {
            LocalTransform = Matrix.CreateScale(0.25f);
            DiffuseColor = new Color(255, 184, 151).ToVector3();
        }
    }
}
