﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace ChrisXue.Pacman.Client
{
    public class Camera : Entity
    {
        public Matrix View { get; set; }
        public Matrix Projection { get; set; }

        private Vector3 Target { get; set; }
        private Vector3 Up { get; set; }

        private MouseState PreviousMouseState { get; set; }

        public Camera()
        {
            var x = Game.Level.Width * Tile.HalfWidth;
            var y = Game.Level.Height * Tile.HalfWidth;
            WorldPosition = new Vector3(x, 50, y);
            Target = new Vector3(x, 0, y);
            Up = Vector3.Forward;
        }

        public override void Update()
        {
            var mouseState = Mouse.GetState();

            var direction = mouseState.ScrollWheelValue - PreviousMouseState.ScrollWheelValue;
            if (direction != 0)
            {
                WorldPosition += new Vector3(0, direction / -30f, 0);
            }

            if (mouseState.LeftButton == ButtonState.Pressed && PreviousMouseState.LeftButton == ButtonState.Pressed)
            {
                var x = mouseState.X - PreviousMouseState.X;
                var y = mouseState.Y - PreviousMouseState.Y;
                var offset = new Vector3(x / -5f, 0, y / -5f);
                WorldPosition += offset;
                Target += offset;
            }

            PreviousMouseState = mouseState;
        }

        public override void Draw()
        {
            View = Matrix.CreateLookAt(WorldPosition, Target, Up);
            Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4,
                Game.Graphics.GraphicsDevice.Viewport.AspectRatio, 0.1f, 1000f);
        }
    }
}
