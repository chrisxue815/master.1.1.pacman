﻿using Microsoft.Xna.Framework;
using Point = ChrisXue.Pacman.Contract.Point;

namespace ChrisXue.Pacman.Client
{
    public class Ghost : DynamicTile
    {
        public Vector3 OriginalColor { get; set; }
        public Vector3 MissingColor { get; set; }
        private float Gradient { get; set; }
        private int GradientDirection { get; set; }

        public Ghost(TileMap tileMap, int x, int y, Vector3 color)
            : base(tileMap, x, y)
        {
            DiffuseColor = color;
            OriginalColor = color;
            MissingColor = Vector3.One - color;
            Gradient = 0;
            GradientDirection = 1;
        }

        public override void Update()
        {
            if (Game.Level.EatenPowerPellet)
            {
                const float speed = 2f;
                Gradient += GradientDirection * speed * Game.ElapsedSeconds;

                if (Gradient > 1)
                {
                    Gradient = 2 - Gradient;
                    GradientDirection = -1;
                }
                else if (Gradient < 0)
                {
                    Gradient = -Gradient;
                    GradientDirection = 1;
                }

                DiffuseColor = OriginalColor + MissingColor * Gradient;
            }
            base.Update();
        }

        public override void Move(Point newDirection)
        {
            if (newDirection.IsReverse(Direction)) return;

            base.Move(newDirection);
        }
    }
}
