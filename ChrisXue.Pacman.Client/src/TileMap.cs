﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ChrisXue.Pacman.Contract;

namespace ChrisXue.Pacman.Client
{
    public class TileMap : Entity, IEnumerable<Tile>
    {
        public const float TileWidth = Tile.Width;

        public List<Pacman> Pacmans { get; set; }
        public List<Ghost> Ghosts { get; set; }

        private int Width { get; set; }
        private int Height { get; set; }

        private StaticTile[,] StaticTiles { get; set; }
        private List<DynamicTile> DynamicTiles { get; set; }

        public TileMap(int width, int height)
        {
            Width = width;
            Height = height;
            Pacmans = new List<Pacman>();
            Ghosts = new List<Ghost>();
            StaticTiles = new StaticTile[width, height];
            DynamicTiles = new List<DynamicTile>();
        }

        public override void LoadContent()
        {
            foreach (var tile in this)
            {
                tile.LoadContent();
            }
        }

        public override void Update()
        {
            foreach (var dynamicTile in DynamicTiles)
            {
                dynamicTile.Update();
            }
        }

        public override void Draw()
        {
            foreach (var tile in this)
            {
                tile.Draw();
            }
        }

        public IEnumerator<Tile> GetEnumerator()
        {
            foreach (var staticTile in StaticTiles)
            {
                if (staticTile != null)
                {
                    yield return staticTile;
                }
            }

            foreach (var dynamicTile in DynamicTiles)
            {
                yield return dynamicTile;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public StaticTile this[int x, int y]
        {
            get { return StaticTiles[x, y]; }
            set { StaticTiles[x, y] = value; }
        }

        public StaticTile this[Point p]
        {
            get { return StaticTiles[p.X, p.Y]; }
            set { StaticTiles[p.X, p.Y] = value; }
        }

        public void Add(DynamicTile tile)
        {
            DynamicTiles.Add(tile);
        }

        public void Add(StaticTile tile)
        {
            StaticTiles[tile.X, tile.Y] = tile;
        }

        public void Add(Pellet pellet)
        {
            StaticTiles[pellet.X, pellet.Y] = pellet;
        }

        public bool CanPassThrough(Point walker, Point obstacle)
        {
            obstacle = Clamp(obstacle);
            var tile = this[obstacle];

            if (tile is Gate)
            {
                return walker.IsBelow(obstacle) || walker.IsHorizontal(obstacle);
            }
            else
            {
                return tile == null || tile.CanPassThrough;
            }
        }

        public int ClampX(int x)
        {
            if (x < 0) x += Width;
            else if (x >= Width) x -= Width;
            return x;
        }

        public int ClampY(int y)
        {
            if (y < 0) y += Height;
            else if (y >= Height) y -= Height;
            return y;
        }

        public Point Clamp(Point p)
        {
            return new Point(ClampX(p.X), ClampY(p.Y));
        }
    }
}
