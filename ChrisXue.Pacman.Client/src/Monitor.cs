﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ChrisXue.Pacman.Client
{
    public class Monitor : Entity
    {
        private Texture2D Texture { get; set; }
        private Rectangle InitialDestination { get; set; }
        private int Margin { get; set; }
        private Rectangle Source { get; set; }

        public Monitor()
        {
            Source = new Rectangle(0, 0, 23, 23);

            var gameWidth = Game.Width;
            var gameHeight = Game.Height;
            var width = (int)(0.075 * gameHeight);

            InitialDestination = new Rectangle((int)(0.08 * gameWidth), (int)(0.85 * gameHeight), width, width);

            Margin = (int)(0.625 * width);
        }

        public override void LoadContent()
        {
            Texture = Game.Content.Load<Texture2D>("tilesets/tileset");
        }

        public override void Draw()
        {
            var destination = new Rectangle[Game.Level.PacmanLives];
            for (var i = 0; i < Game.Level.PacmanLives; i++)
            {
                destination[i] = new Rectangle
                {
                    X = InitialDestination.X + i * (InitialDestination.Width + Margin),
                    Y = InitialDestination.Y,
                    Width = InitialDestination.Width,
                    Height = InitialDestination.Height
                };
            }

            Game.SpriteBatch.Begin();
            for (var i = 0; i < Game.Level.PacmanLives; i++)
            {
                Game.SpriteBatch.Draw(Texture, destination[i], Source, Color.White, 0, Vector2.Zero,
                    SpriteEffects.FlipHorizontally, 0);
            }
            Game.SpriteBatch.End();
        }
    }
}
