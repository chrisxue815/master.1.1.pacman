﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ChrisXue.Pacman.Client
{
    public class Wall : StaticTile
    {
        public Wall(int x, int y) : base(x, y)
        {
            DiffuseColor = new Color(17, 54, 255).ToVector3();
            CanPassThrough = false;
        }
    }
}
