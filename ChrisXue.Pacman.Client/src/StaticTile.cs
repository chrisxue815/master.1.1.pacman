﻿using Microsoft.Xna.Framework;

namespace ChrisXue.Pacman.Client
{
    public class StaticTile : Tile
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        public bool CanPassThrough { get; set; }

        public StaticTile(int x, int y)
        {
            X = x;
            Y = y;
            CanPassThrough = true;
            WorldPosition = new Vector3(Width * x, 0, Width * y);
        }
    }
}
