﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ChrisXue.Pacman.Client
{
    public class Pacman : DynamicTile
    {
        public Pacman(TileMap tileMap, int x, int y) : base(tileMap, x, y)
        {
            DiffuseColor = new Color(255, 251, 26).ToVector3();
        }
    }
}
