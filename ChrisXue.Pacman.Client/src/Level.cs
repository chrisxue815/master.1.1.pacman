﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading;
using ChrisXue.Pacman.Contract;
using Microsoft.Xna.Framework;
using TiledSharp;

namespace ChrisXue.Pacman.Client
{
    [CallbackBehavior(UseSynchronizationContext = false)]
    public class Level : Entity, ILevelServiceCallback
    {
        public int Width { get; set; }
        public int Height { get; set; }

        private TileMap TileMap { get; set; }
        private List<Entity> Entities { get; set; }

        public ILevelService Service { get; set; }

        public bool EatenPowerPellet { get; set; }
        public int PacmanMaxLives { get; set; }
        public int PacmanLives { get; set; }

        public enum TileSetGid
        { Empty, Pacman, Blinky, Inky, Pinky, Clyde, Wall, Gate, Pellet = 11, PowerPellet }

        public Level()
        {
            var map = new TmxMap(Game.StartingInfo.MapPath);
            var tiles = map.Layers[0].Tiles;

            Width = map.Width;
            Height = map.Height;
            TileMap = new TileMap(Width, Height);
            Entities = new List<Entity> { TileMap };

            foreach (var tile in tiles)
            {
                var gid = (TileSetGid)tile.Gid;
                var x = tile.X;
                var y = tile.Y;
                Color? ghostColor = null;

                switch (gid)
                {
                    case TileSetGid.Wall:
                        TileMap.Add(new Wall(x, y));
                        break;
                    case TileSetGid.Pellet:
                        TileMap.Add(new Pellet(x, y));
                        break;
                    case TileSetGid.PowerPellet:
                        TileMap.Add(new PowerPellet(x, y));
                        break;
                    case TileSetGid.Pacman:
                        TileMap.Pacmans.Add(new Pacman(TileMap, x, y));
                        break;
                    case TileSetGid.Blinky:
                        ghostColor = new Color(255, 0, 0);
                        break;
                    case TileSetGid.Inky:
                        ghostColor = new Color(53, 237, 226);
                        break;
                    case TileSetGid.Pinky:
                        ghostColor = new Color(251, 140, 242);
                        break;
                    case TileSetGid.Clyde:
                        ghostColor = new Color(255, 154, 45);
                        break;
                    case TileSetGid.Gate:
                        TileMap.Add(new Gate(x, y));
                        break;
                    default:
                        break;
                }

                if (!ghostColor.HasValue) continue;

                var ghost = new Ghost(TileMap, x, y, ghostColor.Value.ToVector3());
                TileMap.Ghosts.Add(ghost);

                EatenPowerPellet = false;
                PacmanMaxLives = Game.StartingInfo.PacmanMaxLives;
                PacmanLives = PacmanMaxLives;
            }

            Player player = null;
            switch (Game.StartingInfo.Team)
            {
                case 0:
                    player = new Player(TileMap.Pacmans[Game.StartingInfo.Position]);
                    break;
                case 1:
                    player = new Player(TileMap.Ghosts[Game.StartingInfo.Position]);
                    break;
            }
            Entities.Add(player);

            var thread = new Thread(()=>
            {
                Service = ClientFactory.CreateLevelClient(Game.StartingInfo.LevelAddress, this);

                Service.Login(Game.StartingInfo.PlayerId);
            });
            thread.Start();
        }

        public override void LoadContent()
        {
            foreach (var entity in Entities)
            {
                entity.LoadContent();
            }
        }

        public override void Update()
        {
            foreach (var entity in Entities)
            {
                entity.Update();
            }
        }

        public override void Draw()
        {
            foreach (var entity in Entities)
            {
                entity.Draw();
            }
        }

        public void StateChanged(LevelInfo levelInfo)
        {
            foreach (var point in levelInfo.PelletsRemoved)
            {
                TileMap[point] = null;
            }

            for (var i = 0; i < TileMap.Pacmans.Count; i++)
            {
                TileMap.Pacmans[i].Position = levelInfo.Pacmans[i].Position;
                TileMap.Pacmans[i].Direction = levelInfo.Pacmans[i].Direction;
                TileMap.Pacmans[i].Movement = levelInfo.Pacmans[i].Movement;
                TileMap.Pacmans[i].SetWorldPosition();
            }

            for (var i = 0; i < TileMap.Ghosts.Count; i++)
            {
                TileMap.Ghosts[i].Position = levelInfo.Ghosts[i].Position;
                TileMap.Ghosts[i].Direction = levelInfo.Ghosts[i].Direction;
                TileMap.Ghosts[i].Movement = levelInfo.Ghosts[i].Movement;
                TileMap.Ghosts[i].SetWorldPosition();
            }

            var resetColor = EatenPowerPellet && !levelInfo.EatenPowerPellet;
            EatenPowerPellet = levelInfo.EatenPowerPellet;
            if (resetColor)
            {
                foreach (var ghost in TileMap.Ghosts)
                {
                    ghost.DiffuseColor = ghost.OriginalColor;
                }
            }

            PacmanLives = levelInfo.PacmanLives;
        }

        public override void UnloadContent()
        {
            try
            {
                Service.LeaveLobby();
                ((IClientChannel)Service).Close();
            }
            catch (Exception ex)
            {
                ((IClientChannel)Service).Abort();
                Console.WriteLine(ex);
            }
        }
    }
}
