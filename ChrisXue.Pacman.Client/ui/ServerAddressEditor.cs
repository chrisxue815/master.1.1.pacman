﻿using System;
using System.Windows.Forms;

namespace ChrisXue.Pacman.Client.UI
{
    public partial class ServerAddressEditor : Form
    {
        public ServerAddressEditor()
        {
            InitializeComponent();

            var serverAddress = Settings.Default.ServerAddress;
            if (serverAddress != null)
            {
                textServerAddress.Text = serverAddress;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Settings.Default.ServerAddress = textServerAddress.Text;
            Settings.Default.Save();
            Close();
        }

        private void textServerAddress_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (char) Keys.Return) return;

            e.Handled = true;
            buttonOK.PerformClick();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
