﻿using System;
using System.Windows.Forms;

namespace ChrisXue.Pacman.Client.UI
{
    public class BaseControl : UserControl
    {
        protected Form1 Form { get; set; }

        public BaseControl()
        {
            Form = Form1.Instance;
        }

        public void SafeInvoke(Action updater, bool forceSynchronous = true)
        {
            if (InvokeRequired)
            {
                if (forceSynchronous)
                {
                    Invoke((Action)(() => SafeInvoke(updater)));
                }
                else
                {
                    BeginInvoke((Action)(() => SafeInvoke(updater, false)));
                }
            }
            else
            {
                if (IsDisposed)
                {
                    throw new ObjectDisposedException("Control is already disposed.");
                }

                updater();
            }
        }
    }
}
