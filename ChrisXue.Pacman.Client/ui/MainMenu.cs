﻿using System;
using System.ServiceModel;
using System.Windows.Forms;
using ChrisXue.Pacman.Contract;
using ChrisXue.Pacman.Server;
using Microsoft.Xna.Framework;

namespace ChrisXue.Pacman.Client.UI
{
    public partial class MainMenu : BaseControl
    {
        public MainMenu()
        {
            InitializeComponent();

            tableLayoutPanelRight.CellBorderStyle = tableLayoutPanelRight.CellBorderStyle;

            if (Settings.Default.PlayerName == "")
            {
                ShowPlayerNameEditor();
            }
            else
            {
                labelPlayerName.Text = Settings.Default.PlayerName;
            }
        }

        private void ShowPlayerNameEditor()
        {
            var playerNameEditor = new PlayerNameEditor();
            playerNameEditor.ShowDialog(this);
            labelPlayerName.Text = Settings.Default.PlayerName;
        }

        private void buttonSinglePlayer_Click(object sender, EventArgs e)
        {
            Form.ShowGame();
        }

        private void buttonLAN_Click(object sender, EventArgs e)
        {
            Form.Show(new LANMenu());
        }

        private void buttonLobby_Click(object sender, EventArgs e)
        {
            var serverAddressEditor = new ServerAddressEditor();
            var dialogResult = serverAddressEditor.ShowDialog(this);
            if (dialogResult == DialogResult.OK)
            {
                try
                {
                    var serverAddress = Settings.Default.ServerAddress;
                    var uriBuilder = new UriBuilder(serverAddress)
                    {
                        Scheme = "net.tcp",
                        Path = GameServer.Pathes[typeof (IRoomService)]
                    };
                    var endpointAddress = new EndpointAddress(uriBuilder.Uri);
                    var lobbyService = ClientFactory.CreateLobbyClient(endpointAddress);

                    var lobbyMenu = new LobbyMenu();
                    lobbyMenu.Initialize(lobbyService);

                    Form.Show(lobbyMenu);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Form.Close();
        }

        private void buttonEditPlayerName_Click(object sender, EventArgs e)
        {
            ShowPlayerNameEditor();
        }
    }
}
