﻿using System;
using System.ServiceModel;
using System.ServiceModel.Discovery;
using System.Threading;
using ChrisXue.Pacman.Contract;
using ChrisXue.Pacman.Server;

namespace ChrisXue.Pacman.Client.UI
{
    public partial class LANMenu : BaseControl
    {
        public LANMenu()
        {
            InitializeComponent();

            tableLayoutPanelRight.CellBorderStyle = tableLayoutPanelRight.CellBorderStyle;

            FindGameServices();
        }

        private void FindGameServices()
        {
            //TODO: improve this
            var discoveryClient = new DiscoveryClient(new UdpDiscoveryEndpoint());
            discoveryClient.FindProgressChanged += NewServerFound;
            discoveryClient.FindAsync(new FindCriteria(typeof(ILobbyService)));
        }

        private void NewServerFound(object sender, FindProgressChangedEventArgs e)
        {
            var lobbyAddress = e.EndpointDiscoveryMetadata.Address;
            string serverName;

            var lobbyService = ClientFactory.CreateLobbyClient(lobbyAddress);
            using (lobbyService as IClientChannel)
            {
                serverName = lobbyService.ServerName();
            }

            if (serverName == "") return;

            listBoxServer.Items.Add(new ServerInfo
            {
                Name = serverName,
                Address = lobbyAddress
            });

            if (listBoxServer.SelectedIndex == -1) listBoxServer.SelectedIndex = 0;
        }

        private void buttonJoinGame_Click(object sender, EventArgs e)
        {
            var serverInfo = listBoxServer.SelectedItem as ServerInfo;

            if (serverInfo == null) return;

            var lobbyAddress = serverInfo.Address;

            var lobbyService = ClientFactory.CreateLobbyClient(lobbyAddress);
            using (lobbyService as IClientChannel)
            {
                Form.PlayerId = lobbyService.Signup(Settings.Default.PlayerName);
            }

            var room = new Room();
            room.JoinRoom(lobbyAddress);
            Form.Show(room);
        }

        private void listBoxServer_DoubleClick(object sender, EventArgs e)
        {

        }

        private void listBoxServer_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonJoinGame.Enabled = listBoxServer.SelectedIndex != -1;
        }

        private void buttonCreateGame_Click(object sender, EventArgs e)
        {
            if (Form.GameServer != null)
            {
                Form.GameServer.Closed += (o, args) => CreateRoom();
                Form.GameServer.Close();
            }
            
            CreateRoom();
        }

        private void CreateRoom()
        {
            Form.ServerThread = new Thread(() =>
            {
                Form.GameServer = new GameServer();

                Form.GameServer.Opened += (sender1, eventArgs) =>
                {
                    var lobbyAddress = Form.GameServer.Addresses[typeof(ILobbyService)];

                    var lobbyService = ClientFactory.CreateLobbyClient(lobbyAddress);
                    using (lobbyService as IClientChannel)
                    {
                        //TODO: improve lan server creation, prevent other clients creating first room
                        Form.PlayerId = lobbyService.Signup(Settings.Default.PlayerName);
                    }

                    var room = new Room();
                    room.CreateRoom(lobbyAddress);
                    SafeInvoke(() => Form.Show(room));
                };

                Form.GameServer.Open();
            });

            Form.ServerThread.Start();
        }

        private void buttonReturn_Click(object sender, EventArgs e)
        {
            Form.ShowMainMenu();
        }

        private class ServerInfo
        {
            public string Name { get; set; }
            public EndpointAddress Address { get; set; }

            public override string ToString()
            {
                return Name;
            }
        }
    }
}
