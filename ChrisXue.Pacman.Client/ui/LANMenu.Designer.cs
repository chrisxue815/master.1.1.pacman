﻿namespace ChrisXue.Pacman.Client.UI
{
    partial class LANMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanelBackground = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelRight = new System.Windows.Forms.TableLayoutPanel();
            this.buttonCreateGame = new System.Windows.Forms.Button();
            this.buttonReturn = new System.Windows.Forms.Button();
            this.buttonJoinGame = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.listBoxServer = new System.Windows.Forms.ListBox();
            this.tableLayoutPanelBackground.SuspendLayout();
            this.tableLayoutPanelRight.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelBackground
            // 
            this.tableLayoutPanelBackground.ColumnCount = 2;
            this.tableLayoutPanelBackground.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.05417F));
            this.tableLayoutPanelBackground.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.94583F));
            this.tableLayoutPanelBackground.Controls.Add(this.tableLayoutPanelRight, 1, 0);
            this.tableLayoutPanelBackground.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanelBackground.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelBackground.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelBackground.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelBackground.Name = "tableLayoutPanelBackground";
            this.tableLayoutPanelBackground.Padding = new System.Windows.Forms.Padding(30, 16, 30, 16);
            this.tableLayoutPanelBackground.RowCount = 1;
            this.tableLayoutPanelBackground.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelBackground.Size = new System.Drawing.Size(1024, 576);
            this.tableLayoutPanelBackground.TabIndex = 3;
            // 
            // tableLayoutPanelRight
            // 
            this.tableLayoutPanelRight.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanelRight.ColumnCount = 1;
            this.tableLayoutPanelRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelRight.Controls.Add(this.buttonCreateGame, 0, 4);
            this.tableLayoutPanelRight.Controls.Add(this.buttonReturn, 0, 8);
            this.tableLayoutPanelRight.Controls.Add(this.buttonJoinGame, 0, 3);
            this.tableLayoutPanelRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelRight.Location = new System.Drawing.Point(626, 16);
            this.tableLayoutPanelRight.Margin = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.tableLayoutPanelRight.Name = "tableLayoutPanelRight";
            this.tableLayoutPanelRight.RowCount = 10;
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.Size = new System.Drawing.Size(368, 544);
            this.tableLayoutPanelRight.TabIndex = 0;
            // 
            // buttonCreateGame
            // 
            this.buttonCreateGame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonCreateGame.Location = new System.Drawing.Point(9, 225);
            this.buttonCreateGame.Margin = new System.Windows.Forms.Padding(8);
            this.buttonCreateGame.Name = "buttonCreateGame";
            this.buttonCreateGame.Size = new System.Drawing.Size(350, 37);
            this.buttonCreateGame.TabIndex = 1;
            this.buttonCreateGame.Text = "Create Game";
            this.buttonCreateGame.UseVisualStyleBackColor = true;
            this.buttonCreateGame.Click += new System.EventHandler(this.buttonCreateGame_Click);
            // 
            // buttonReturn
            // 
            this.buttonReturn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonReturn.Location = new System.Drawing.Point(9, 441);
            this.buttonReturn.Margin = new System.Windows.Forms.Padding(8);
            this.buttonReturn.Name = "buttonReturn";
            this.buttonReturn.Size = new System.Drawing.Size(350, 37);
            this.buttonReturn.TabIndex = 3;
            this.buttonReturn.Text = "Return";
            this.buttonReturn.UseVisualStyleBackColor = true;
            this.buttonReturn.Click += new System.EventHandler(this.buttonReturn_Click);
            // 
            // buttonJoinGame
            // 
            this.buttonJoinGame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonJoinGame.Enabled = false;
            this.buttonJoinGame.Location = new System.Drawing.Point(9, 171);
            this.buttonJoinGame.Margin = new System.Windows.Forms.Padding(8);
            this.buttonJoinGame.Name = "buttonJoinGame";
            this.buttonJoinGame.Size = new System.Drawing.Size(350, 37);
            this.buttonJoinGame.TabIndex = 0;
            this.buttonJoinGame.Text = "Join Game";
            this.buttonJoinGame.UseVisualStyleBackColor = true;
            this.buttonJoinGame.Click += new System.EventHandler(this.buttonJoinGame_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.listBoxServer);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(30, 16);
            this.panel1.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(578, 544);
            this.panel1.TabIndex = 1;
            // 
            // listBoxServer
            // 
            this.listBoxServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxServer.FormattingEnabled = true;
            this.listBoxServer.ItemHeight = 25;
            this.listBoxServer.Location = new System.Drawing.Point(0, 0);
            this.listBoxServer.Margin = new System.Windows.Forms.Padding(0);
            this.listBoxServer.Name = "listBoxServer";
            this.listBoxServer.Size = new System.Drawing.Size(578, 544);
            this.listBoxServer.TabIndex = 1;
            this.listBoxServer.SelectedIndexChanged += new System.EventHandler(this.listBoxServer_SelectedIndexChanged);
            this.listBoxServer.DoubleClick += new System.EventHandler(this.listBoxServer_DoubleClick);
            // 
            // LANMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanelBackground);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "LANMenu";
            this.Size = new System.Drawing.Size(1024, 576);
            this.tableLayoutPanelBackground.ResumeLayout(false);
            this.tableLayoutPanelRight.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelBackground;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelRight;
        private System.Windows.Forms.Button buttonReturn;
        private System.Windows.Forms.Button buttonCreateGame;
        private System.Windows.Forms.Button buttonJoinGame;
        private System.Windows.Forms.ListBox listBoxServer;
        private System.Windows.Forms.Panel panel1;


    }
}
