﻿namespace ChrisXue.Pacman.Client.UI
{
    partial class Room
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanelAll = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelRight = new System.Windows.Forms.TableLayoutPanel();
            this.buttonReturn = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.tableLayoutPanelLeft = new System.Windows.Forms.TableLayoutPanel();
            this.comboBoxPacman1 = new System.Windows.Forms.ComboBox();
            this.linkLabelPacman = new System.Windows.Forms.LinkLabel();
            this.comboBoxGhost4 = new System.Windows.Forms.ComboBox();
            this.comboBoxGhost3 = new System.Windows.Forms.ComboBox();
            this.comboBoxGhost2 = new System.Windows.Forms.ComboBox();
            this.comboBoxGhost1 = new System.Windows.Forms.ComboBox();
            this.linkLabelGhosts = new System.Windows.Forms.LinkLabel();
            this.comboBoxPacman2 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.comboBoxMap = new System.Windows.Forms.ComboBox();
            this.labelMap = new System.Windows.Forms.Label();
            this.tableLayoutPanelAll.SuspendLayout();
            this.tableLayoutPanelRight.SuspendLayout();
            this.tableLayoutPanelLeft.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelAll
            // 
            this.tableLayoutPanelAll.ColumnCount = 2;
            this.tableLayoutPanelAll.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.05417F));
            this.tableLayoutPanelAll.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.94583F));
            this.tableLayoutPanelAll.Controls.Add(this.tableLayoutPanelRight, 1, 0);
            this.tableLayoutPanelAll.Controls.Add(this.tableLayoutPanelLeft, 0, 0);
            this.tableLayoutPanelAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelAll.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelAll.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelAll.Name = "tableLayoutPanelAll";
            this.tableLayoutPanelAll.Padding = new System.Windows.Forms.Padding(30, 16, 30, 16);
            this.tableLayoutPanelAll.RowCount = 1;
            this.tableLayoutPanelAll.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelAll.Size = new System.Drawing.Size(1024, 576);
            this.tableLayoutPanelAll.TabIndex = 4;
            // 
            // tableLayoutPanelRight
            // 
            this.tableLayoutPanelRight.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanelRight.ColumnCount = 1;
            this.tableLayoutPanelRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelRight.Controls.Add(this.buttonReturn, 0, 8);
            this.tableLayoutPanelRight.Controls.Add(this.buttonStart, 0, 4);
            this.tableLayoutPanelRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelRight.Location = new System.Drawing.Point(626, 16);
            this.tableLayoutPanelRight.Margin = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.tableLayoutPanelRight.Name = "tableLayoutPanelRight";
            this.tableLayoutPanelRight.RowCount = 10;
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanelRight.Size = new System.Drawing.Size(368, 544);
            this.tableLayoutPanelRight.TabIndex = 0;
            // 
            // buttonReturn
            // 
            this.buttonReturn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonReturn.Location = new System.Drawing.Point(9, 441);
            this.buttonReturn.Margin = new System.Windows.Forms.Padding(8);
            this.buttonReturn.Name = "buttonReturn";
            this.buttonReturn.Size = new System.Drawing.Size(353, 37);
            this.buttonReturn.TabIndex = 3;
            this.buttonReturn.Text = "Leave Room";
            this.buttonReturn.UseVisualStyleBackColor = true;
            this.buttonReturn.Click += new System.EventHandler(this.buttonReturn_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonStart.Location = new System.Drawing.Point(9, 225);
            this.buttonStart.Margin = new System.Windows.Forms.Padding(8);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(353, 37);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "Start Game";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // tableLayoutPanelLeft
            // 
            this.tableLayoutPanelLeft.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanelLeft.ColumnCount = 3;
            this.tableLayoutPanelLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.861314F));
            this.tableLayoutPanelLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.27007F));
            this.tableLayoutPanelLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.72263F));
            this.tableLayoutPanelLeft.Controls.Add(this.comboBoxPacman1, 1, 1);
            this.tableLayoutPanelLeft.Controls.Add(this.linkLabelPacman, 1, 0);
            this.tableLayoutPanelLeft.Controls.Add(this.comboBoxGhost4, 1, 7);
            this.tableLayoutPanelLeft.Controls.Add(this.comboBoxGhost3, 1, 6);
            this.tableLayoutPanelLeft.Controls.Add(this.comboBoxGhost2, 1, 5);
            this.tableLayoutPanelLeft.Controls.Add(this.comboBoxGhost1, 1, 4);
            this.tableLayoutPanelLeft.Controls.Add(this.linkLabelGhosts, 1, 3);
            this.tableLayoutPanelLeft.Controls.Add(this.comboBoxPacman2, 1, 2);
            this.tableLayoutPanelLeft.Controls.Add(this.button1, 0, 4);
            this.tableLayoutPanelLeft.Controls.Add(this.button2, 0, 5);
            this.tableLayoutPanelLeft.Controls.Add(this.button3, 0, 6);
            this.tableLayoutPanelLeft.Controls.Add(this.button4, 0, 7);
            this.tableLayoutPanelLeft.Controls.Add(this.button5, 0, 1);
            this.tableLayoutPanelLeft.Controls.Add(this.button6, 0, 2);
            this.tableLayoutPanelLeft.Controls.Add(this.comboBoxMap, 1, 10);
            this.tableLayoutPanelLeft.Controls.Add(this.labelMap, 1, 9);
            this.tableLayoutPanelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelLeft.Location = new System.Drawing.Point(30, 16);
            this.tableLayoutPanelLeft.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.tableLayoutPanelLeft.Name = "tableLayoutPanelLeft";
            this.tableLayoutPanelLeft.RowCount = 12;
            this.tableLayoutPanelLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333334F));
            this.tableLayoutPanelLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333334F));
            this.tableLayoutPanelLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333334F));
            this.tableLayoutPanelLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333334F));
            this.tableLayoutPanelLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333334F));
            this.tableLayoutPanelLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333334F));
            this.tableLayoutPanelLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333334F));
            this.tableLayoutPanelLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333334F));
            this.tableLayoutPanelLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333334F));
            this.tableLayoutPanelLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333334F));
            this.tableLayoutPanelLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333334F));
            this.tableLayoutPanelLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333334F));
            this.tableLayoutPanelLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanelLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanelLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanelLeft.Size = new System.Drawing.Size(578, 544);
            this.tableLayoutPanelLeft.TabIndex = 1;
            // 
            // comboBoxPacman1
            // 
            this.comboBoxPacman1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxPacman1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPacman1.FormattingEnabled = true;
            this.comboBoxPacman1.Items.AddRange(new object[] {
            "Open",
            "Closed",
            "AI",
            ""});
            this.comboBoxPacman1.Location = new System.Drawing.Point(43, 48);
            this.comboBoxPacman1.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxPacman1.Name = "comboBoxPacman1";
            this.comboBoxPacman1.Size = new System.Drawing.Size(221, 33);
            this.comboBoxPacman1.TabIndex = 1;
            this.comboBoxPacman1.SelectedIndexChanged += new System.EventHandler(this.comboBoxPacman1_SelectedIndexChanged);
            // 
            // linkLabelPacman
            // 
            this.linkLabelPacman.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.linkLabelPacman.AutoSize = true;
            this.linkLabelPacman.Location = new System.Drawing.Point(43, 10);
            this.linkLabelPacman.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.linkLabelPacman.Name = "linkLabelPacman";
            this.linkLabelPacman.Size = new System.Drawing.Size(85, 25);
            this.linkLabelPacman.TabIndex = 11;
            this.linkLabelPacman.TabStop = true;
            this.linkLabelPacman.Text = "PacMan";
            this.linkLabelPacman.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelPacman_LinkClicked);
            // 
            // comboBoxGhost4
            // 
            this.comboBoxGhost4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxGhost4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxGhost4.FormattingEnabled = true;
            this.comboBoxGhost4.Items.AddRange(new object[] {
            "Open",
            "Closed",
            "AI",
            ""});
            this.comboBoxGhost4.Location = new System.Drawing.Point(43, 318);
            this.comboBoxGhost4.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxGhost4.Name = "comboBoxGhost4";
            this.comboBoxGhost4.Size = new System.Drawing.Size(221, 33);
            this.comboBoxGhost4.TabIndex = 6;
            this.comboBoxGhost4.SelectedIndexChanged += new System.EventHandler(this.comboBoxGhost4_SelectedIndexChanged);
            // 
            // comboBoxGhost3
            // 
            this.comboBoxGhost3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxGhost3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxGhost3.FormattingEnabled = true;
            this.comboBoxGhost3.Items.AddRange(new object[] {
            "Open",
            "Closed",
            "AI",
            ""});
            this.comboBoxGhost3.Location = new System.Drawing.Point(43, 273);
            this.comboBoxGhost3.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxGhost3.Name = "comboBoxGhost3";
            this.comboBoxGhost3.Size = new System.Drawing.Size(221, 33);
            this.comboBoxGhost3.TabIndex = 5;
            this.comboBoxGhost3.SelectedIndexChanged += new System.EventHandler(this.comboBoxGhost3_SelectedIndexChanged);
            // 
            // comboBoxGhost2
            // 
            this.comboBoxGhost2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxGhost2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxGhost2.FormattingEnabled = true;
            this.comboBoxGhost2.Items.AddRange(new object[] {
            "Open",
            "Closed",
            "AI",
            ""});
            this.comboBoxGhost2.Location = new System.Drawing.Point(43, 228);
            this.comboBoxGhost2.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxGhost2.Name = "comboBoxGhost2";
            this.comboBoxGhost2.Size = new System.Drawing.Size(221, 33);
            this.comboBoxGhost2.TabIndex = 4;
            this.comboBoxGhost2.SelectedIndexChanged += new System.EventHandler(this.comboBoxGhost2_SelectedIndexChanged);
            // 
            // comboBoxGhost1
            // 
            this.comboBoxGhost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxGhost1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxGhost1.FormattingEnabled = true;
            this.comboBoxGhost1.Items.AddRange(new object[] {
            "Open",
            "Closed",
            "AI",
            ""});
            this.comboBoxGhost1.Location = new System.Drawing.Point(43, 183);
            this.comboBoxGhost1.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxGhost1.Name = "comboBoxGhost1";
            this.comboBoxGhost1.Size = new System.Drawing.Size(221, 33);
            this.comboBoxGhost1.TabIndex = 3;
            this.comboBoxGhost1.SelectedIndexChanged += new System.EventHandler(this.comboBoxGhost1_SelectedIndexChanged);
            // 
            // linkLabelGhosts
            // 
            this.linkLabelGhosts.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.linkLabelGhosts.AutoSize = true;
            this.linkLabelGhosts.Location = new System.Drawing.Point(43, 145);
            this.linkLabelGhosts.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.linkLabelGhosts.Name = "linkLabelGhosts";
            this.linkLabelGhosts.Size = new System.Drawing.Size(74, 25);
            this.linkLabelGhosts.TabIndex = 12;
            this.linkLabelGhosts.TabStop = true;
            this.linkLabelGhosts.Text = "Ghosts";
            this.linkLabelGhosts.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelGhosts_LinkClicked);
            // 
            // comboBoxPacman2
            // 
            this.comboBoxPacman2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxPacman2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPacman2.FormattingEnabled = true;
            this.comboBoxPacman2.Items.AddRange(new object[] {
            "Open",
            "Closed",
            "AI",
            ""});
            this.comboBoxPacman2.Location = new System.Drawing.Point(43, 93);
            this.comboBoxPacman2.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxPacman2.Name = "comboBoxPacman2";
            this.comboBoxPacman2.Size = new System.Drawing.Size(221, 33);
            this.comboBoxPacman2.TabIndex = 15;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(3, 183);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(35, 33);
            this.button1.TabIndex = 7;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(237)))), ((int)(((byte)(226)))));
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(3, 228);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(35, 33);
            this.button2.TabIndex = 8;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(140)))), ((int)(((byte)(242)))));
            this.button3.Dock = System.Windows.Forms.DockStyle.Top;
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(3, 273);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(35, 33);
            this.button3.TabIndex = 9;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(154)))), ((int)(((byte)(45)))));
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.Enabled = false;
            this.button4.Location = new System.Drawing.Point(3, 318);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(35, 33);
            this.button4.TabIndex = 10;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Yellow;
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.Enabled = false;
            this.button5.Location = new System.Drawing.Point(3, 48);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(35, 33);
            this.button5.TabIndex = 16;
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Yellow;
            this.button6.Dock = System.Windows.Forms.DockStyle.Top;
            this.button6.Enabled = false;
            this.button6.Location = new System.Drawing.Point(3, 93);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(35, 33);
            this.button6.TabIndex = 17;
            this.button6.UseVisualStyleBackColor = false;
            // 
            // comboBoxMap
            // 
            this.comboBoxMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxMap.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMap.FormattingEnabled = true;
            this.comboBoxMap.Items.AddRange(new object[] {
            "Pac-Man 1980",
            "Ms. Pac-Man",
            "Google Pac-Man"});
            this.comboBoxMap.Location = new System.Drawing.Point(43, 453);
            this.comboBoxMap.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxMap.Name = "comboBoxMap";
            this.comboBoxMap.Size = new System.Drawing.Size(221, 33);
            this.comboBoxMap.TabIndex = 14;
            // 
            // labelMap
            // 
            this.labelMap.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelMap.AutoSize = true;
            this.labelMap.Location = new System.Drawing.Point(43, 415);
            this.labelMap.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelMap.Name = "labelMap";
            this.labelMap.Size = new System.Drawing.Size(51, 25);
            this.labelMap.TabIndex = 13;
            this.labelMap.Text = "Map";
            // 
            // Room
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanelAll);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Room";
            this.Size = new System.Drawing.Size(1024, 576);
            this.tableLayoutPanelAll.ResumeLayout(false);
            this.tableLayoutPanelRight.ResumeLayout(false);
            this.tableLayoutPanelLeft.ResumeLayout(false);
            this.tableLayoutPanelLeft.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelRight;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonReturn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelAll;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelLeft;
        private System.Windows.Forms.ComboBox comboBoxPacman1;
        private System.Windows.Forms.ComboBox comboBoxGhost1;
        private System.Windows.Forms.ComboBox comboBoxGhost2;
        private System.Windows.Forms.ComboBox comboBoxGhost3;
        private System.Windows.Forms.ComboBox comboBoxGhost4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.LinkLabel linkLabelPacman;
        private System.Windows.Forms.LinkLabel linkLabelGhosts;
        private System.Windows.Forms.Label labelMap;
        private System.Windows.Forms.ComboBox comboBoxMap;
        private System.Windows.Forms.ComboBox comboBoxPacman2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;

    }
}
