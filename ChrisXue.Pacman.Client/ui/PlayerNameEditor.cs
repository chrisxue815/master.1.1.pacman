﻿using System;
using System.Windows.Forms;

namespace ChrisXue.Pacman.Client.UI
{
    public partial class PlayerNameEditor : Form
    {
        public PlayerNameEditor()
        {
            InitializeComponent();

            var playerName = Settings.Default.PlayerName;
            if (playerName != null)
            {
                textPlayerName.Text = playerName;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Settings.Default.PlayerName = textPlayerName.Text;
            Settings.Default.Save();
            Close();
        }

        private void textPlayerName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (char) Keys.Return) return;

            e.Handled = true;
            buttonOK.PerformClick();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
