﻿using System.Windows.Forms;

namespace ChrisXue.Pacman.Client.UI
{
    partial class MainMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanelBackground = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelRight = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.labelWelcome = new System.Windows.Forms.Label();
            this.buttonOptions = new System.Windows.Forms.Button();
            this.buttonLobby = new System.Windows.Forms.Button();
            this.buttonLAN = new System.Windows.Forms.Button();
            this.buttonSinglePlayer = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.labelPlayerName = new System.Windows.Forms.Label();
            this.buttonEditPlayerName = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.tableLayoutPanelBackground.SuspendLayout();
            this.tableLayoutPanelRight.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelBackground
            // 
            this.tableLayoutPanelBackground.ColumnCount = 2;
            this.tableLayoutPanelBackground.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.05417F));
            this.tableLayoutPanelBackground.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.94583F));
            this.tableLayoutPanelBackground.Controls.Add(this.tableLayoutPanelRight, 1, 0);
            this.tableLayoutPanelBackground.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelBackground.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelBackground.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelBackground.Name = "tableLayoutPanelBackground";
            this.tableLayoutPanelBackground.Padding = new System.Windows.Forms.Padding(30, 16, 30, 16);
            this.tableLayoutPanelBackground.RowCount = 1;
            this.tableLayoutPanelBackground.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelBackground.Size = new System.Drawing.Size(1024, 576);
            this.tableLayoutPanelBackground.TabIndex = 2;
            // 
            // tableLayoutPanelRight
            // 
            this.tableLayoutPanelRight.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanelRight.ColumnCount = 1;
            this.tableLayoutPanelRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelRight.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanelRight.Controls.Add(this.buttonOptions, 0, 6);
            this.tableLayoutPanelRight.Controls.Add(this.buttonLobby, 0, 5);
            this.tableLayoutPanelRight.Controls.Add(this.buttonLAN, 0, 4);
            this.tableLayoutPanelRight.Controls.Add(this.buttonSinglePlayer, 0, 3);
            this.tableLayoutPanelRight.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanelRight.Controls.Add(this.buttonExit, 0, 8);
            this.tableLayoutPanelRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelRight.Location = new System.Drawing.Point(626, 16);
            this.tableLayoutPanelRight.Margin = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.tableLayoutPanelRight.Name = "tableLayoutPanelRight";
            this.tableLayoutPanelRight.RowCount = 10;
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.tableLayoutPanelRight.Size = new System.Drawing.Size(368, 544);
            this.tableLayoutPanelRight.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90.88472F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.115281F));
            this.tableLayoutPanel4.Controls.Add(this.labelWelcome, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(9, 63);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(8);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 68F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(350, 37);
            this.tableLayoutPanel4.TabIndex = 8;
            // 
            // labelWelcome
            // 
            this.labelWelcome.AutoSize = true;
            this.labelWelcome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelWelcome.Location = new System.Drawing.Point(2, 0);
            this.labelWelcome.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelWelcome.Name = "labelWelcome";
            this.labelWelcome.Size = new System.Drawing.Size(314, 37);
            this.labelWelcome.TabIndex = 0;
            this.labelWelcome.Text = "Welcome";
            this.labelWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonOptions
            // 
            this.buttonOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonOptions.Enabled = false;
            this.buttonOptions.Location = new System.Drawing.Point(9, 333);
            this.buttonOptions.Margin = new System.Windows.Forms.Padding(8);
            this.buttonOptions.Name = "buttonOptions";
            this.buttonOptions.Size = new System.Drawing.Size(350, 37);
            this.buttonOptions.TabIndex = 3;
            this.buttonOptions.Text = "Options";
            this.buttonOptions.UseVisualStyleBackColor = true;
            // 
            // buttonLobby
            // 
            this.buttonLobby.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonLobby.Location = new System.Drawing.Point(9, 279);
            this.buttonLobby.Margin = new System.Windows.Forms.Padding(8);
            this.buttonLobby.Name = "buttonLobby";
            this.buttonLobby.Size = new System.Drawing.Size(350, 37);
            this.buttonLobby.TabIndex = 2;
            this.buttonLobby.Text = "Connect to Game Lobby";
            this.buttonLobby.UseVisualStyleBackColor = true;
            this.buttonLobby.Click += new System.EventHandler(this.buttonLobby_Click);
            // 
            // buttonLAN
            // 
            this.buttonLAN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonLAN.Location = new System.Drawing.Point(9, 225);
            this.buttonLAN.Margin = new System.Windows.Forms.Padding(8);
            this.buttonLAN.Name = "buttonLAN";
            this.buttonLAN.Size = new System.Drawing.Size(350, 37);
            this.buttonLAN.TabIndex = 1;
            this.buttonLAN.Text = "LAN";
            this.buttonLAN.UseVisualStyleBackColor = true;
            this.buttonLAN.Click += new System.EventHandler(this.buttonLAN_Click);
            // 
            // buttonSinglePlayer
            // 
            this.buttonSinglePlayer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSinglePlayer.Enabled = false;
            this.buttonSinglePlayer.Location = new System.Drawing.Point(9, 171);
            this.buttonSinglePlayer.Margin = new System.Windows.Forms.Padding(8);
            this.buttonSinglePlayer.Name = "buttonSinglePlayer";
            this.buttonSinglePlayer.Size = new System.Drawing.Size(350, 37);
            this.buttonSinglePlayer.TabIndex = 0;
            this.buttonSinglePlayer.Text = "Single Player";
            this.buttonSinglePlayer.UseVisualStyleBackColor = true;
            this.buttonSinglePlayer.Click += new System.EventHandler(this.buttonSinglePlayer_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90.88472F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.115281F));
            this.tableLayoutPanel2.Controls.Add(this.labelPlayerName, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.buttonEditPlayerName, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(9, 117);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(8);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 68F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(350, 37);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // labelPlayerName
            // 
            this.labelPlayerName.AutoSize = true;
            this.labelPlayerName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPlayerName.Location = new System.Drawing.Point(2, 0);
            this.labelPlayerName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPlayerName.Name = "labelPlayerName";
            this.labelPlayerName.Size = new System.Drawing.Size(314, 37);
            this.labelPlayerName.TabIndex = 1;
            this.labelPlayerName.Text = "Player Name";
            this.labelPlayerName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonEditPlayerName
            // 
            this.buttonEditPlayerName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonEditPlayerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEditPlayerName.Location = new System.Drawing.Point(321, 5);
            this.buttonEditPlayerName.Margin = new System.Windows.Forms.Padding(2);
            this.buttonEditPlayerName.Name = "buttonEditPlayerName";
            this.buttonEditPlayerName.Size = new System.Drawing.Size(26, 26);
            this.buttonEditPlayerName.TabIndex = 2;
            this.buttonEditPlayerName.Text = "E";
            this.buttonEditPlayerName.UseVisualStyleBackColor = true;
            this.buttonEditPlayerName.Click += new System.EventHandler(this.buttonEditPlayerName_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonExit.Location = new System.Drawing.Point(9, 441);
            this.buttonExit.Margin = new System.Windows.Forms.Padding(8);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(350, 37);
            this.buttonExit.TabIndex = 7;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanelBackground);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MainMenu";
            this.Size = new System.Drawing.Size(1024, 576);
            this.tableLayoutPanelBackground.ResumeLayout(false);
            this.tableLayoutPanelRight.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelRight;
        private System.Windows.Forms.Button buttonSinglePlayer;
        private System.Windows.Forms.Button buttonLAN;
        private System.Windows.Forms.Button buttonLobby;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label labelWelcome;
        private System.Windows.Forms.Label labelPlayerName;
        private System.Windows.Forms.Button buttonEditPlayerName;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonOptions;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelBackground;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
    }
}
