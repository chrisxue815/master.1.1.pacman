﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using ChrisXue.Pacman.Contract;
using ChrisXue.Pacman.Server;

namespace ChrisXue.Pacman.Client.UI
{
    [CallbackBehavior(UseSynchronizationContext = false)]
    public partial class Room : BaseControl, IRoomServiceCallback
    {
        private EndpointAddress RoomAddress { get; set; }
        private IRoomService RoomService { get; set; }
        private List<List<ComboBox>> ComboBoxTeams { get; set; }
        private bool Hosting { get; set; }
        private int Team { get; set; }
        private int Position { get; set; }

        public Room()
        {
            InitializeComponent();

            tableLayoutPanelRight.CellBorderStyle = tableLayoutPanelRight.CellBorderStyle;

            ComboBoxTeams = new List<List<ComboBox>>
            {
                new List<ComboBox> { comboBoxPacman1, comboBoxPacman2 },
                new List<ComboBox> {comboBoxGhost1, comboBoxGhost2, comboBoxGhost3, comboBoxGhost4}
            };

            foreach (var comboBox in ComboBoxTeams.SelectMany(comboBoxTeam => comboBoxTeam))
            {
                comboBox.SelectedIndex = 2;
            }

            comboBoxMap.SelectedIndex = 2;

            Hosting = false;
        }

        public void CreateRoom(EndpointAddress lobbyAddress)
        {
            Hosting = true;

            foreach (var comboBox in ComboBoxTeams.SelectMany(comboBoxTeam => comboBoxTeam))
            {
                comboBox.Enabled = false;
            }

            comboBoxMap.Enabled = true;
            buttonStart.Enabled = true;

            RoomAddress = GetRoomAddress(lobbyAddress);
            RoomService = ClientFactory.CreateRoomClient(RoomAddress, this);

            RoomService.Login(Form.PlayerId);
            var roomInfo = RoomService.CreateRoom();

            foreach (var slotInfo in roomInfo.Slots)
            {
                Apply(slotInfo);
                if (slotInfo.Player.Id != Form.PlayerId) continue;
                Team = slotInfo.TeamId;
                Position = slotInfo.Position;
            }
        }

        public void JoinRoom(EndpointAddress lobbyAddress)
        {
            Hosting = false;

            foreach (var comboBox in ComboBoxTeams.SelectMany(comboBoxTeam => comboBoxTeam))
            {
                comboBox.Enabled = false;
            }

            comboBoxMap.Enabled = false;
            buttonStart.Enabled = false;

            RoomAddress = GetRoomAddress(lobbyAddress);
            RoomService = ClientFactory.CreateRoomClient(RoomAddress, this);

            RoomService.Login(Form.PlayerId);
            var roomInfo = RoomService.JoinFirstRoom();

            foreach (var slotInfo in roomInfo.Slots)
            {
                Apply(slotInfo);
                if (slotInfo.Player.Id != Form.PlayerId) continue;
                Team = slotInfo.TeamId;
                Position = slotInfo.Position;
            }
        }

        private static EndpointAddress GetRoomAddress(EndpointAddress lobbyAddress)
        {
            var roomPath = GameServer.Pathes[typeof(IRoomService)];
            var uriBuilder = new UriBuilder(lobbyAddress.Uri) { Path = roomPath };
            var uri = uriBuilder.Uri;
            var roomAddress = new EndpointAddress(uri);
            return roomAddress;
        }

        private void Apply(RoomSlotInfo slotInfo)
        {
            SafeInvoke(() =>
            {
                var comboBox = ComboBoxTeams[slotInfo.TeamId][slotInfo.Position];
                comboBox.Items[3] = slotInfo.Player;
                comboBox.SelectedIndex = 3;
                comboBox.Enabled = Hosting && slotInfo.Player.Id != Form.PlayerId;
            });
        }

        public void PlayerJoinedRoom(RoomSlotInfo slotInfo)
        {
            Apply(slotInfo);
        }

        public void PlayerLeftRoom(int id)
        {
            throw new NotImplementedException();
        }

        public void PlayerChangedPosition(RoomSlotInfo slotInfo)
        {
            throw new NotImplementedException();
        }

        public void GameStarted(GameStartingInfo startingInfo)
        {
            SafeInvoke(() =>
            {
                Form.ShowGame();
                var clientStartingInfo = new GameClientStartingInfo
                {
                    MapPath = startingInfo.MapPath,
                    NumPacman = startingInfo.NumPacman,
                    NumGhosts = startingInfo.NumGhosts,
                    RoomAddress = RoomAddress,
                    Team = Team,
                    Position = Position,
                    PlayerId = Form.PlayerId
                };
                Form.Game.Initialize(clientStartingInfo);
            });
        }

        private void linkLabelPacman_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void linkLabelGhosts_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void comboBoxPacman1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxGhost1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxGhost2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxGhost3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxGhost4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonReturn_Click(object sender, EventArgs e)
        {
            RoomService.LeaveRoom();

            ((IClientChannel)RoomService).Dispose();

            if (Form.GameServer != null) Form.GameServer.Close();

            Form.Show(new LANMenu());
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            RoomService.StartGame(comboBoxMap.SelectedIndex);
        }
    }
}
