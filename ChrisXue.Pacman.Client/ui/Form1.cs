﻿using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using ChrisXue.Pacman.Server;

namespace ChrisXue.Pacman.Client.UI
{
    public partial class Form1 : Form
    {
        public static Form1 Instance { get; set; }

        public Game1 Game { get; set; }
        public Panel GamePanel { get; set; }

        public GameServer GameServer { get; set; }
        public Thread ServerThread { get; set; }
        public int PlayerId { get; set; }

        private FormState InitialState { get; set; }

        public Form1()
        {
            Instance = this;
            Application.EnableVisualStyles();

            InitializeComponent();

            InitialState = new FormState
            {
                Size = Size,
                FormBorderStyle = FormBorderStyle,
                FormWindowState = WindowState
            };

            GamePanel = new Panel
            {
                Location = new System.Drawing.Point(0, 0)
            };

            Game = new Game1(this);
            Closing += (sender, args) =>
            {
                Game.Quit();
                if (GameServer != null)
                {
                    var thread = new Thread(() => GameServer.Close());
                    thread.Start();
                }
            };

            ShowMainMenu();

            Show();
            Game.Run();
        }

        public void Show(Control control)
        {
            Controls.Clear();
            Controls.Add(control);
        }

        public void ShowMainMenu()
        {
            Size = InitialState.Size;
            FormBorderStyle = InitialState.FormBorderStyle;
            WindowState = InitialState.FormWindowState;

            var mainMenu = new MainMenu();
            Show(mainMenu);
        }

        public void ShowGame()
        {
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;

            var screen = Screen.FromControl(this);
            var resolution = screen.Bounds;

            GamePanel.Size = new Size(resolution.Width, resolution.Height);
            Show(GamePanel);

            //Game.Initialize();
        }

        private class FormState
        {
            public Size Size { get; set; }
            public FormBorderStyle FormBorderStyle { get; set; }
            public FormWindowState FormWindowState { get; set; }
        }
    }
}
